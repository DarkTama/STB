<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\move;
// use App\Http\Controllers\view;
use Validator;
use App\upload;
use App\pic;
use App\kontak;
use App\slider;
use App\Gambar;
use App\Produk;
use App\klien;
use App\Services;
use App\Blog;
use App\Profiles;
use App\Testimonials;
use App\Office;
use App\Jenis;
use App\Video;
use App\Projek;
use App\Unduhan;
use App\Partner;
use App\UserWEB;
use App\JenisUser;

class uploade extends Controller
{
    function admin(Request $req){
        $jenis = Jenis::all();
        $profiles = Profiles::all();
        $kontak = kontak::all();
        $sosmed = pic::all();
        $office = Office::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('welcomes', ['profiles'=>$profiles, 'kontak'=>$kontak, 'sosmed'=>$sosmed, 'office'=>$office, 'jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function inisialisasi(){
        $jenis = Jenis::all();
        // $sosmed = pic::all();
        // $kontak = 
        // $jenis = Jenis::all();
        // $video = Video::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('inisialisasi', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function pps(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        return view('pps', ['jenis'=>$jenis, 'gambar'=>$gambar]);
    }
    function editProf(Request $req){
        // $prof = new Profiles;
        // $prof->id_prof = $req->id;
        $prof = Profiles::find($req->id);
        $prof->slogan = $req->slogan;
        $prof->highlight = $req->highlight;
        $prof->profil = $req->profil;
        $prof->save();
        return response()->json($prof);
    }
    function arrayKontak(){
        $datan = kontak::all();
        $baris = array();
        $a = 0;
        foreach ($datan as $datas){
            $baris[$a]['id_ktk'] = $datas->id_ktk;
            // $baris[$a]['id'] = ($a+1);
            $baris[$a]['jnskontak'] = $datas->jnskontak;
            $baris[$a]['dtlkontak'] = $datas->dtlkontak;
            $baris[$a]['ikon'] = $datas->ikon;
            $baris[$a]['urut'] = $datas->urut;
            $a++;
        }
        $data = array('data'=>$baris);
        return response()->json($data);
    }
    function arraySosmed(){
        $datan = pic::all();
        $baris = array();
        $a = 0;
        foreach ($datan as $datas){
            $baris[$a]['id_sosmed'] = $datas->id_sosmed;
            // $baris[$a]['id'] = ($a+1);
            $baris[$a]['jnssosmed'] = $datas->jnssosmed;
            $baris[$a]['dtlsosmed'] = $datas->dtlsosmed;
            $baris[$a]['ikon'] = $datas->ikon;
            $baris[$a]['urut'] = $datas->urut;
            $a++;
        }
        $data = array('data'=>$baris);
        return response()->json($data);
    }
    function arrayAlamat(){
        $datan = Office::all();
        $baris = array();
        $a = 0;
        foreach ($datan as $datas){
            $baris[$a]['id_off'] = $datas->id_off;
            // $baris[$a]['id'] = ($a+1);
            $baris[$a]['nama'] = $datas->nama;
            $baris[$a]['alamat'] = $datas->alamat;
            $baris[$a]['kota'] = $datas->kota;
            $baris[$a]['kodepos'] = $datas->kodepos;
            $baris[$a]['telp'] = $datas->telp;
            $baris[$a]['kordinat'] = $datas->kordinat;
            $baris[$a]['aktif'] = $datas->aktif;
            $a++;
        }
        $data = array('data'=>$baris);
        return response()->json($data);
    }
    function arrayJenis(){
        $jenis = Jenis::all();
        $row = array();
        $q = 0;
        foreach ($jenis as $jen){
            $row[$q]['id_jps'] = $jen->id_jps;
            $row[$q]['jps'] = $jen->jps;
            $q++;
        }
        $data = array('data'=>$row);
        return response()->json($data);
    }
    function arrayVideo(){
        $video = Video::all();
        $row = array();
        $q = 0;
        foreach ($video as $vid){
            $row[$q]['id_vid'] = $vid->id_vid;
            $row[$q]['judulvid'] = $vid->judulvid;
            $row[$q]['linkvid'] = $vid->linkvid;
            $q++;
        }
        $data = array('data'=>$row);
        return response()->json($data);
    }
    function arraySlider(){
        $slider = slider::all();
        // $row = array();
        // $q = 0;
        // foreach ($slider as $sli){
        //     $row[$q]['id_sld'] = $sli->id_sld;
        //     $row[$q]['judulsld'] = $sli->judulsld;
        //     $row[$q]['subjudul'] = $sli->subjudul;
        //     $row[$q]['prakata'] = $sli->prakata;
        //     $row[$q]['deskripsi'] = $sli->deskripsi;
        //     $row[$q]['posisi'] = $sli->posisi;
        //     $row[$q]['urut'] = $sli->urut;
        //     $row[$q]['aktif'] = $sli->aktif;
        //     $row[$q]['lokasi'] = slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('gbr.lokasi')->get();
        //     $q++;
        // }
        $row = slider::leftjoin('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('slider.*', 'gbr.*')->select('gbr.lokasi', 'slider.*')->get();
        $data = array('data'=>$row);
        return response()->json($data);
    }
    function arrayProduk(){
        $produk = Produk::leftjoin('jns_prosrv', 'jns_prosrv.id_jps', '=' ,'produk.id_jps')->select('jns_prosrv.jps', 'produk.*')->get();
        // $produk = Produk::leftjoin('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->join('jns_prosrv', 'jns_prosrv.id_jps', '=' ,'produk.id_jps')->get();
        $data = array('data'=>$produk);
        return response()->json($data);
    }
    function arrayProjek(){
        $projek = Projek::leftjoin('jns_prosrv', 'jns_prosrv.id_jps', '=', 'projek.id_jps')->select('projek.*', 'jns_prosrv.jps')->get();
        $data = array('data'=>$projek);
        return response()->json($data);
    }
    function arrayServis(){
        $servis = Services::leftjoin('jns_prosrv', 'jns_prosrv.id_jps', '=', 'servis.id_jps')->select('jns_prosrv.jps', 'servis.*')->get();
        $data = array('data'=>$servis);
        return response()->json($data);
    }
    // function arrayBlog(){
    //     $blog = Blog::leftjoin('gbr', 'gbr.id_blog', '=', 'blog.id_blog')->get();
    //     $data = array('data'=>$blog);
    //     return response()->json($data);
    // }
    // function arrayKlienTesti(){
    //     $klientesti = klien::join('')
    // }
    function arrayPartner(){
        $partner = Partner::leftjoin('gbr', 'gbr.id_partner', '=', 'partner.id_partner')->select('gbr.lokasi', 'partner.*')->get();
        $data = array('data'=>$partner);
        return response()->json($data);
    }
    function arrayUnduhan(){
        $unduhan = Unduhan::all();
        $data = array('data'=>$unduhan);
        return response()->json($data);
    }
    function arrayPicture(Request $req){
        // $gbr = $req->get('id');
        $gbr = $req->id;
        $jenis = Jenis::all();
        $data['gambar'] = Gambar::where('id_prod', $gbr)->get();
        // foreach($data as $dat){
        //     $gb = '<img id="pictures" src=/liravel/public/'.$dat->lokasi.'>';
        // }
        return response()->json($data);
        // return $gb;
        // return view('produk.produk', $data, ['jenis'=>$jenis]);
    }
    function arrayBlog(){
        $blog = Blog::leftjoin('usr_web', 'usr_web.id_usr', '=', 'blog.rec_usr')->select('blog.*', 'usr_web.namauser')->select('usr_web.namauser', 'blog.*')->get();
        $data = array('data'=>$blog);
        return response()->json($data);
    }
    function arrayKlien(){
        $klien = klien::leftjoin('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.lokasi', 'klien.*')->get();
        $data = array('data'=>$klien);
        return response()->json($data);
    }
    function arrayTesti(){
        $testi = Testimonials::leftjoin('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->leftjoin('klien', 'klien.id_klien', '=', 'testi.id_klien')->select('gbr.lokasi', 'klien.klien', 'testi.*')->get();
        $data = array('data'=>$testi);
        return response()->json($data);
    }
    function arrayUserWEB(){
        $users = UserWEB::leftjoin('gbr', 'gbr.id_usr', '=', 'usr_web.id_usr')->leftjoin('jns_usr', 'jns_usr.id_jns', '=', 'usr_web.jnsuser')->select('gbr.lokasi', 'jns_usr.jenisuser', 'usr_web.*')->get();
        $data = array('data'=>$users);
        return response()->json($data); 
    }
    function tambahKontak(Request $req){
        $pekl = new kontak;
        $pekl->id_ktk = $req->id;
        $pekl->jnskontak = $req->jenis;
        $pekl->dtlkontak = $req->kontak;
        $pekl->ikon = $req->ikon;
        $pekl->urut = $req->urut;
        $pekl->save();
        return response()->json($pekl);    
    }
    function editKontak(Request $req, $id){
        $kon = kontak::find($req->id);
        $kon->jnskontak = $req->jenis;
        $kon->dtlkontak = $req->kontak;
        $kon->ikon = $req->ikon;
        $kon->urut = $req->urut;
        $kon->save();
        return response()->json($kon);
    }
    function hapusKontak(Request $req){
        $kon = kontak::find($req->id)->delete();
        // $kon = kontak::where('id_ktk', $id)
        // $kon = kontak::destroy($id);
        // $kon = kontak::destroy($id);
        return response()->json($kon);
    }
    function tambahSosmed(Request $req){
        $pekl = new pic;
        $pekl->id_sosmed = $req->id;
        $pekl->jnssosmed = $req->jeniss;
        $pekl->dtlsosmed = $req->sosmed;
        $pekl->ikon = $req->ikons;
        $pekl->urut = $req->uruts;
        $pekl->save();
        return response()->json($pekl);    
    }
    function editSosmed(Request $req){
        $sos = pic::find($req->id);
        $sos->jnssosmed = $req->jenis;
        $sos->dtlsosmed = $req->sosmed;
        $sos->ikon = $req->ikon;
        $sos->urut = $req->urut;
        $sos->save();
        return response()->json($sos);
    }
    function hapusSosmed(Request $req){
        $sos = pic::find($req->id)->delete();
        return response()->json($sos);
    }
    function tambahOffice(Request $req){
        $off = new Office;
        $off->id_off = $req->id;
        $off->nama = $req->nama;
        $off->alamat = $req->alamat;
        $off->kota = $req->kota;
        $off->kodepos = $req->kodepos;
        $off->kordinat = $req->koordinat;
        $off->telp = $req->telephone;
        $off->aktif = $req->aktif;
        $off->save();
        return response()->json($off);
    }
    function editOffice(Request $req){
        $off = Office::find($req->id);
        // $off->id_off = $req->id;
        $off->nama = $req->nama;
        $off->alamat = $req->alamat;
        $off->kota = $req->kota;
        $off->kodepos = $req->kodepos;
        $off->kordinat = $req->koordinat;
        $off->telp = $req->telephone;
        $off->aktif = $req->aktif;
        $off->save();
        return response()->json($off);
    }
    function hapusOffice(Request $req){
        $off = Office::find($req->id)->delete();
        return response()->json($off);
    }
    function tambahJenis(Request $req){
        $jps = new Jenis;
        $jps->id_jps = $req->id;
        $jps->jps = $req->jenis;
        $jps->save();
        return response()->json($jps);
    }
    function editJenis(Request $req){
        $jps = Jenis::find($req->id);
        $jps->jps = $req->jenis;
        $jps->save();
        return response()->json($jps);
    }
    function hapusJenis(Request $req){
        $jps = Jenis::find($req->id)->delete();
        return response()->json($jps);
    }
    function tambahVideo(Request $req){
        $vid = new Video;
        $vid->id_vid = $req->id;
        $vid->judulvid = $req->judul;
        $vid->linkvid = $req->link;
        $vid->save();
        return response()->json($vid);
    }
    function editVideo(Request $req){
        $vid = Video::find($req->id);
        $vid->judulvid = $req->judul;
        $vid->linkvid = $req->link;
        $vid->save();
        return response()->json($vid);
    }
    function hapusVideo(Request $req){
        $vid = Video::find($req->id)->delete();
        return response()->json($vid);
    }
    function tambahSlider(Request $req){
        // $sld = DB::transaction(function() use($req){
        //     DB::table('slider')->insert([
                // 'judulsld'=>$req->judul,
                // 'subjudul'=>$req->subjudul,
                // 'prakata'=>$req->prakata,
                // 'posisigambar'=>$req->posisigambar,
                // 'deskripsi'=>$req->deskripsi,
                // 'posisi'=>$req->posisiteks,
                // 'urut'=>$req->urut,
                // 'aktif'=>$req->aktif,
        //     ]);
        //     DB::table('gbr')->insert([
        //         'lokasi'=>$req->lokasi,
        //         'id_sld'=>$req->id,
        //     ]);
        // });
        // $sld = Slider::create([
        //     'judulsld'=>$req->judul,
        //     'subjudul'=>$req->subjudul,
        //     'prakata'=>$req->prakata,
        //     'posisigambar'=>$req->posisigambar,
        //     'deskripsi'=>$req->deskripsi,
        //     'posisi'=>$req->posisiteks,
        //     'urut'=>$req->urut,
        //     'aktif'=>$req->aktif,
        // ]);
        $sld = new slider;
        $sld->judulsld = $req->judul;
        $sld->subjudul = $req->subjudul;
        $sld->prakata = $req->prakata;
        $sld->deskripsi = $req->deskripsi;
        $sld->posisigambar = $req->posisigambar;
        $sld->posisi = $req->posisiteks;
        $sld->urut = $req->urut;
        $sld->aktif = $req->aktif;
        $gbr = new Gambar;
        $gbr->id_sld = $sld->id;
        // $pic = Input::file('picturesld');
        // $pic->move(public_path(). 'img/slides', $pic->time());
        // $gbr->lokasi = $pic;
        $sld->save();
        $sld->gambar()->save($gbr);
        // $sld->save();
        // $sld = new Gambar;
        // $sld->lokasi = $req->lokasi;
        // $sld->id_sld = $req->id;
        // $sld->save();
        // $gbr->save();
        return response()->json($sld);
    }
    function uploadSlider(Request $req){
        $text = $req->picturesldid;
        $gbr = $req->file('picturesldup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->picturesldup->move(public_path('img/slides'), $name);
        $up = Gambar::where('id_sld', $req->picturesldid);
        $up->update(['lokasi'=>'img/slides/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('inisialisasi', ['jenis'=>$jenis, 'gambar'=>$gambar, 'asd'=>$asd, 'gbr'=>$gbr, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@inisialisasi');
    }
    function editSlider(Request $req){
        $sld = slider::find($req->id);
        $sld->judulsld = $req->judul;
        $sld->subjudul = $req->subjudul;
        $sld->prakata = $req->prakata;
        $sld->deskripsi = $req->deskripsi;
        $sld->posisigambar = $req->posisigambar;
        $sld->posisi = $req->posisiteks;
        $sld->urut = $req->urut;
        $sld->aktif = $req->aktif;
        $sld->save();
        return response()->json($sld);   
    }
    function hapusSlider(Request $req){
        $sld = slider::find($req->id)->delete();
        // $gbr = Gambar::find('id_sld', $req->id)->delete();
        return response()->json($sld);
    }
    function produk(Request $req){
        $jenis = Jenis::all();
        $asd = $req->input('pictureShowsID');
        // $asd = Input::get('#pictureShowsID');
        $gbr = $req->id;
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        // $gambar = Gambar::where('id_prod', 10);
        return view('produk.produk', ['jenis'=>$jenis, 'gambar'=>$gambar, 'asd'=>$asd, 'gbr'=>$gbr, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahProduk(Request $req){
        $prod = new Produk;
        $prod->id_jps = $req->jps;
        $prod->produk = $req->produk;
        $prod->deskripsi = $req->deskripsi;
        $prod->save();
        return response()->json($prod);
    }
    function editProduk(Request $req){
        $prod = Produk::find($req->id);
        $prod->id_jps = $req->jps;
        $prod->produk = $req->produk;
        $prod->deskripsi = $req->deskripsi;
        $prod->save();
        return response()->json($prod);
    }
    function hapusProduk(Request $req){
        $prod = Produk::find($req->id)->delete();
        return response()->json($prod);
    }
    function uploadProduk(Request $req){
        $text = $req->pictureprodukid;
        $gbr = $req->file('pictureprodukup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->pictureprodukup->move(public_path('img/showcase'), $name);
        // $up = Gambar::where('id', $req->pictureprodukid);
        $up = new Gambar;
        $up->lokasi = 'img/showcase/'.$name;
        $up->id_prod = $text;
        $up->save();
        // $up->create(['lokasi'=>'img/showcase/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('produk.produk', ['jenis'=>$jenis, 'gambar'=>$gambar, 'gbr'=>$gbr, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@produk');
    }
    function uploadProduks(Request $req){
        $allowed=['png', 'jpg', 'jpeg'];
        $files = $req->file('pictureprodukups');
        foreach($files as $file){
            $extension = $file->getClientOriginalExtension();
            $filename = $file->getClientOriginalName().'.'.$extension;
            $check= in_array($extension, $allowed);
            if($check){
                foreach($req->pictureprodukups as $photo){
                    $filename = $photo->store('pictureprodukups');
                    Gambar::create([
                        'lokasi'=> 'img/showcase/'.$filename,
                        'id_prod'=> $req->pictureprodukids,
                    ]);
                }
            }
        }
    }
    function hapusProdukGambar(Request $req){
        $prodgbr = Gambar::where('id_prod', $req->id)->delete();
        return response()->json($prodgbr);    
    }
    function projek(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('projek.projek', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahProjek(Request $req){
        $proj = new Projek;
        $proj->id_jps = $req->jps;
        $proj->projek = $req->projek;
        $proj->deskripsi = $req->deskripsi;
        $gbr = new Gambar;
        $proj->save();
        $proj->Gambar()->save($gbr);
        return response()->json($proj);
    }
    function editProjek(Request $req){
        $proj = Projek::find($req->id);
        $proj->id_jps = $req->jps;
        $proj->projek = $req->projek;
        $proj->deskripsi = $req->deskripsi;
        $proj->save();
        return response()->json($proj);
    }
    function hapusProjek(Request $req){
        $proj = Projek::find($req->id)->delete();
        return response()->json($proj);
    }
    function uploadProjek(Request $req){
        $text = $req->pictureprojekid;
        $gbr = $req->file('pictureprojekup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->pictureprojekup->move(public_path('img/showcase'), $name);
        // $up = Gambar::where('id', $req->pictureprodukid);
        $up = new Gambar;
        $up->lokasi = 'img/showcase/'.$name;
        $up->id_prjt = $text;
        $up->save();
        // $up->create(['lokasi'=>'img/showcase/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('projek.projek', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@projek');
    }
    function servis(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('servis.servis', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahServis(Request $req){
        $serv = new Services;
        $serv->id_jps = $req->jps;
        $serv->servis = $req->servis;
        $serv->deskripsi = $req->deskripsi;
        $serv->ikon = $req->ikon;
        $serv->save();
        return response()->json($serv);
    }
    function editServis(Request $req){
        $serv = Services::find($req->id);
        $serv->id_jps = $req->jps;
        $serv->servis = $req->servis;
        $serv->deskripsi = $req->deskripsi;
        $serv->ikon = $req->ikon;
        $serv->save();
        return response()->json($serv);
    }
    function hapusServis(Request $req){
        $serv = Services::find($req->id)->delete();
        return response()->json($serv);
    }
    function blog(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('blog.blog', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahBlog(Request $req){
        $blog = new Blog;
        $blog->judul = $req->judul;
        $blog->isi_blog = $req->isi;
        $blog->rec_usr = $req->penulis;
        $gbr = new Gambar;
        $gbr->id_blog = $blog->id;
        $blog->save();
        $blog->Gambar()->save($gbr);
        return response()->json($blog);
    }
    function editBlog(Request $req){
        $blog = Blog::find($req->id);
        $blog->judul = $req->judul;
        $blog->isi_blog = $req->isi;
        $blog->rec_usr = $req->penulis;
        $blog->save();
        return response()->json($blog);
    }
    function hapusBlog(Request $req){
        $blog = Blog::find($req->id)->delete();
        return response()->json($blog);
    }
    function uploadBlog(Request $req){
        $text = $req->pictureblogid;
        $gbr = $req->file('pictureblogup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->pictureblogup->move(public_path('img/blog'), $name);
        $up = new Gambar;
        $up->lokasi = 'img/blog/'.$name;
        $up->id_blog = $text;
        $up->save();
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('blog.blog', ['jenis'=>$jenis, 'gambar'=>$gambar, 'gbr'=>$gbr, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@blog');
    }
    function klientesti(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('klientesti.klientesti', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahKlien(Request $req){
        $klien = new klien;
        $klien->klien = $req->klien;
        $klien->alamat = $req->alamat;
        $klien->telp = $req->telp;
        $klien->email = $req->email;
        $gbr = new Gambar;
        $gbr->id_klien = $klien->id;
        $klien->save();
        $klien->Gambar()->save($gbr);
        return response()->json($klien);
    }
    function editKlien(Request $req){
        $klien = klien::find($req->id);
        $klien->klien = $req->klien;
        $klien->alamat = $req->alamat;
        $klien->telp = $req->telp;
        $klien->email = $req->email;
        $klien->save();
        return response()->json($klien);
    }
    function hapusKlien(Request $req){
        $klien = klien::find($req->id)->delete();
        return response()->json($klien);
    }
    function uploadKlien(Request $req){
        $text = $req->pictureklienid;
        $gbr = $req->file('pictureklienup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->pictureklienup->move(public_path('img/clients'), $name);
        $up = Gambar::where('id_klien', $req->pictureklienid);
        $up->update(['lokasi'=>'img/clients/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('klientesti.klientesti', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@klientesti');
    }
    function tambahTesti(Request $req){
        $testi = new Testimonials;
        $testi->id_klien = $req->klien;
        $testi->nama = $req->nama;
        $testi->testimoni = $req->testi;
        $gbr = new Gambar;
        $gbr->id_testi = $testi->id;
        $testi->save();
        $testi->Gambar()->save($gbr);
        return response()->json($testi);
    }
    function editTesti(Request $req){
        $testi = Testimonials::find($req->id);
        $testi->id_klien = $req->klien;
        $testi->nama = $req->nama;
        $testi->testimoni = $req->testi;
        $testi->save();
        return response()->json($testi);
    }
    function hapusTesti(Request $req){
        $testi = Testimonials::find($req->id)->delete();
        return response()->json($testi);
    }
    function uploadTesti(Request $req){
        $text = $req->picturetestiid;
        $gbr = $req->file('picturetestiup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->picturetestiup->move(public_path('img/testi'), $name);
        $up = Gambar::where('id_testi', $req->picturetestiid);
        $up->update(['lokasi'=>'img/testi/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('klientesti.klientesti', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@klientesti');
    }
    function partner(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('partner.partner', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahPartner(Request $req){
        $partner = new Partner;
        $partner->partner = $req->partner;
        $partner->alamat = $req->alamat;
        $partner->telp = $req->telp;
        $partner->email = $req->email;
        $gbr = new Gambar;
        $gbr->id_partner = $partner->id;
        $partner->save();
        $partner->Gambar()->save($gbr);
        return response()->json($partner);
    }
    function editPartner(Request $req){
        $partner = Partner::find($req->id);
        $partner->partner = $req->partner;
        $partner->alamat = $req->alamat;
        $partner->telp = $req->telp;
        $partner->email = $req->email;
        $partner->save();
        return response()->json($partner);
    }
    function hapusPartner(Request $req){
        $partner = Partner::find($req->id)->delete();
        return response()->json($partner);
    }
    function uploadPartner(Request $req){
        $text = $req->picturepartnerid;
        $gbr = $req->file('picturepartnerup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->picturepartnerup->move(public_path('img/partner'), $name);
        $up = Gambar::where('id_partner', $req->picturepartnerid);
        $up->update(['lokasi'=>'img/partner/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('partner.partner', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@partner');
    }
    function unduhan(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('unduhan.unduhan', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahUnduhan(Request $req){
        $unduhan = new Unduhan;
        $unduhan->judulunduh = $req->judul;
        $unduhan->linkunduh = $req->link;
        $unduhan->save();
        return response()->json($unduhan);
    }
    function editUnduhan(Request $req){
        $unduhan = Unduhan::find($req->id);
        $unduhan->judulunduh = $req->judul;
        $unduhan->linkunduh = $req->link;
        $unduhan->save();
        return response()->json($unduhan);
    }
    function hapusUnduhan(Request $req){
        $unduhan = Unduhan::find($req->id)->delete();
        return response()->json($unduhan);
    }
    function userWEB(){
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        $projek = Projek::all();
        return view('userweb.userweb', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser, 'projek'=>$projek]);
    }
    function tambahUsers(Request $req){
        $user = new UserWEB;
        $user->email = $req->email;
        $user->namauser = $req->nama;
        $user->status = $req->status;
        $user->jnsuser = $req->jenis;
        $gbr = new Gambar;
        $gbr->id_usr = $user->id;
        $user->save();
        $user->gambar()->save($gbr);
        return response()->json($user);
    }
    function editUsers(Request $req){
        $user = UserWEB::find($req->id);
        $user->email = $req->email;
        $user->namauser = $req->nama;
        $user->status = $req->status;
        $user->jnsuser = $req->jenis;
        $user->save();
        return response()->json($user);
    }
    function hapusUsers(Request $req){
        $user = UserWEB::find($req->id)->delete();
        return response()->json($user);
    }
    function uploadUsers(Request $req){
        $text = $req->pictureusersid;
        $gbr = $req->file('pictureusersup');
        $name = time().'.'.$gbr->getClientOriginalExtension();
        $req->pictureusersup->move(public_path('img/users'), $name);
        $up = Gambar::where('id_usr', $req->pictureusersid);
        $up->update(['lokasi'=>'img/users/'.$name]);
        $jenis = Jenis::all();
        $gambar = Gambar::all();
        $produk = Produk::all();
        $users = UserWEB::all();
        $blog = Blog::all();
        $klien = klien::all();
        $jenisuser = JenisUser::all();
        // return view('userweb.userweb', ['jenis'=>$jenis, 'gambar'=>$gambar, 'produk'=>$produk, 'users'=>$users, 'blog'=>$blog, 'klien'=>$klien, 'jenisuser'=>$jenisuser]);
        return redirect()->action('uploade@userWEB');
    }
    public function uploadFile()
    {
    	return view('uploadfile');
    }

    public function StoreUploadFile(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'nama' => 'required',
        'content' => 'required',
        'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);

      if ($validator->passes()) {
        $input = $request->all();
        // $input->content = $request->content;
        $input['gambar'] = time().'.'.$request->gambar->getClientOriginalExtension();
        $request->gambar->move(public_path('gambar'), $input['gambar']);

        pic::create($input);
        return response()->json(['success'=>'Berhasil']);
      }

      return response()->json(['error'=>$validator->errors()->all()]);
    }
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function changePassword(Request $request){
 
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        // $validatedData = $request->validate([
        //     'current-password' => 'required',
        //     'new-password' => 'required|string|min:6|confirmed',
        // ]);
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
 
        return redirect()->back()->with("success","Password changed successfully !");
 
    }
    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }
}
