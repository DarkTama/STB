<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\pic;
use App\kontak;
use App\slider;
use App\Gambar;
use App\Produk;
use App\klien;
use App\Services;
use App\Blog;
use App\Profiles;
use App\Testimonials;
use App\Office;
use App\Jenis;
use App\Video;

class WebView extends Controller
{
    function elements(){
        $show = pic::orderby('urut')->get();
        $kontak = kontak::orderby('urut')->get();
        // $gambar = Gambar::all();
        $slider = slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();
        // $produk = Gambar::join('produk', 'produk.id_prod', '=', 'gbr.id_prod')->select('produk.*', 'gbr.lokasi')->get();
        // $produk = produk::join('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
        // $produk = produk::with('gambar')->get();
        $produk = produk::all()->shuffle();
        $klien = klien::join('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
        $services = Services::all()->shuffle();
        $blog = Blog::orderby('rec_wkt', 'desc')->get();
        $profiles = Profiles::all();
        $testi = Testimonials::join('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->join('klien', 'klien.id_klien', '=', 'testi.id_klien')->get()->shuffle();
        // $slider = slider::with('Gambar')->get();
        // $slider = slider::where('rec_sta', 1)->first();
        // dd($slider);
        // $gambar = gambar::all();
        return view('elements', ['show'=>$show, 'kontak'=>$kontak, 'slider'=>$slider, 'produk'=>$produk, 'klien'=>$klien, 'services'=>$services, 'blog'=>$blog, 'profiles'=>$profiles, 'testi'=>$testi]);
    }
    function show(){
    	$show = pic::orderby('urut')->get();
        $kontak = kontak::orderby('urut')->get();
        // $gambar = Gambar::all();
        $slider = slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->orderby('urut')->get();
        // $produk = Gambar::join('produk', 'produk.id_prod', '=', 'gbr.id_prod')->select('produk.*', 'gbr.lokasi')->get();
        // $produk = produk::join('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
        // $produk = produk::with('gambar')->get();
        $produk = produk::all()->shuffle();
        $klien = klien::join('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
        $services = Services::all()->shuffle();
        $blog = Blog::orderby('rec_wkt', 'desc')->get();
        $profiles = Profiles::all();
        $testi = Testimonials::join('gbr', 'gbr.id_testi', '=', 'testi.id_testi')->join('klien', 'klien.id_klien', '=', 'testi.id_klien')->get()->shuffle();
        // $slider = slider::with('Gambar')->get();
        // $slider = slider::where('rec_sta', 1)->first();
        // dd($slider);
        // $gambar = gambar::all();
    	return view('indek', ['show'=>$show, 'kontak'=>$kontak, 'slider'=>$slider, 'produk'=>$produk, 'klien'=>$klien, 'services'=>$services, 'blog'=>$blog, 'profiles'=>$profiles, 'testi'=>$testi]);
    }
    function bout(){
        $show = pic::all();
        $kontak = kontak::all();
        // $gambar = Gambar::all();
        // $slider = slider::join('gbr', 'gbr.id_sld', '=', 'slider.id_sld')->select('gbr.*', 'slider.*')->get();
        // $produk = produk::join('gbr', 'gbr.id_prod', '=', 'produk.id_prod')->select('gbr.*', 'produk.*')->get()->shuffle();
        $klien = klien::join('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
        $profiles = Profiles::join('gbr', 'gbr.id_prof', '=', 'profil.id_prof')->select('gbr.*', 'profil.*')->get();
        // $services = Services::all();
        // $blog = Blog::all();
        // $slider = slider::with('Gambar')->get();
        // $slider = slider::where('rec_sta', 1)->first();
        // dd($slider);
        // $gambar = gambar::all();
        // , 'slider'=>$slider, 'produk'=>$produk, 'services'=>$services, 'blog'=>$blog
        return view('about', ['show'=>$show, 'kontak'=>$kontak, 'klien'=>$klien, 'profiles'=>$profiles]);
    }
    function blog($id){
        $show = pic::all();
        $kontak = kontak::all();
        $klien = klien::join('gbr', 'gbr.id_klien', '=', 'klien.id_klien')->select('gbr.*', 'klien.*')->get()->shuffle();
        $blog = Blog::find($id);
        $blogs = Blog::join('usr_web', 'usr_web.id_usr', '=', 'blog.rec_usr')->select('usr_web.*', 'blog.*')->get();
        return View('blog', ['blog'=>$blog, 'show'=>$show, 'kontak'=>$kontak, 'klien'=>$klien, 'blogs'=>$blogs]);
    }
}
