<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class klien extends Model
{
    protected $table = 'klien';
    protected $primaryKey = 'id_klien';
    public $timestamps = false;
    function Gambar(){
    	return $this->hasOne('App\Gambar', 'id_klien');
    }
}
