<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table = 'slider';
    protected $primaryKey = 'id_sld';
    public $timestamps = false;
    function gambar(){
    	return $this->hasOne('App\gambar', 'id_sld');
    }
}