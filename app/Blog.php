<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $primaryKey = 'id_blog';
    protected $table = 'blog';
    public $timestamps = false;
    function Gambar(){
    	return $this->hasMany('App\Gambar', 'id_blog');
    }
}
