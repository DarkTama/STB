<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projek extends Model
{
    protected $table = 'projek';
    protected $primaryKey = 'id_prjt';
    public $timestamps = false;
    function Gambar(){
    	return $this->hasMany('App\Gambar', 'id_prjt');
    }
}
