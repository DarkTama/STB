<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $table = 'testi';
    protected $primaryKey = 'id_testi';
    public $timestamps = false;
    function Gambar(){
    	return $this->hasOne('App\Gambar', 'id_testi');
    }
}
