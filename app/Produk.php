<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'id_prod';
    public $timestamps = false;
    public function Gambar(){
    	return $this->hasMany('App\Gambar', 'id_prod');
    }
}
