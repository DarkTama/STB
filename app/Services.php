<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'servis';
    protected $primaryKey = 'id_serv';
    public $timestamps = false;
}
