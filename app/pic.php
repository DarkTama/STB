<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pic extends Model
{
	protected $primaryKey = 'id_sosmed';
    protected $table = 'sosmed';
    public $timestamps = false;
}
