<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
	protected $primaryKey = 'id_off';
    protected $table = 'office';
    public $timestamps = false;
}
