<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = 'jns_prosrv';
    protected $primaryKey = 'id_jps';
    public $timestamps = false;

}
