<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kontak extends Model
{
	protected $primaryKey = 'id_ktk';
    protected $table = 'kontak';
    public $timestamps = false;
}
