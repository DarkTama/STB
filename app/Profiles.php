<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
	protected $primaryKey = 'id_prof';
    protected $table = 'profil';
    public $timestamps = false;
}
