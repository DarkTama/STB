<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
	protected $table = 'partner';
    protected $primaryKey = 'id_partner';
    public $timestamps = false;
    function Gambar(){
    	return $this->hasOne('App\Gambar', 'id_partner');
    }
}
