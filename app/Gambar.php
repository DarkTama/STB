<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = 'gbr';
    public $timestamps = false;
    protected $fillable = ['lokasi', 'id_sld', 'id_prod', 'id_prjt'];
    // protected $primaryKey = 'id_gambar';
    // protected $guarded = 'id_gambar';
    function gambarslider(){
    	return $this->belongsTo('App\slider', 'id_sld');
    }
    function gambarklien(){
    	return $this->belongsTo('App\klien', 'id_klien');
    }
    function gambartesti(){
        return $this->belongsTo('App\Testimonials', 'id_testi');
    }
    function gambarblog(){
        return $this->belongsTo('App\Blog', 'id_blog');
    }
    function gambarpartner(){
        return $this->belongsTo('App\Partner', 'id_partner');
    }
    public function Produk(){
    	return $this->belongsTo('App\Produk', 'id_prod', 'id_prod');
    }
    function Projek(){
        return $this->belongsTo('App\Projek', 'id_prjt', 'id_prjt');
    }
}
