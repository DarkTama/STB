<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'link_video';
    protected $primaryKey = 'id_vid';
    public $timestamps = false;
}
