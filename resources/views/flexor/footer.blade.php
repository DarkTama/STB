<footer id="footer" class="block block-bg-grey-dark" data-block-bg-img="{{asset('img/bg_footer-map.png')}}" data-stellar-background-ratio="0.4">
    <div class="container">
      <div class="row subfooter">
        {{-- @todo: replace with company copyright details --}}
        <div class="col-md-7">
          <p>Copyright © Flexor Theme</p>
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Flexor
            -->
            <a href="https://bootstrapmade.com/">Free Bootstrap Templates</a> by BootstrapMade.com
            <address>
              <strong>Flexor Bootstrap Theme Inc</strong>
              @if(count($kontak)>0)
              @foreach($kontak->all() as $kon)
              <br>
              <i class="fa {{$kon->ikon}}"></i> {{$kon->dtlkontak}} 
              {{-- <br>
              <i class="fa fa-phone fa-fw text-primary"></i> 019223 8092344
              <br>
              <i class="fa fa-envelope-o fa-fw text-primary"></i> info@flexorinc.com
              <br> --}}
              @endforeach
              @endif
            </address>
          </div>
        </div>
        <div class="col-md-5">
          <ul class="list-inline pull-right">
            <li><a href="#">Terms</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Contact Us</a></li>
            <li>
              @if(count($show)>0)
            @foreach($show->all() as $sow)
            <a href="{{$sow->dtlsosmed}}"><i class="fa {{$sow->ikon}}"></i></a>
            {{-- <a href="https://facebook.com/darktama"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="https://plus.google.com/+EkatamaIlhamPrayoga"><i class="fa fa-google-plus"></i></a> --}}
            @endforeach
            @endif
            {{--   <a href="https://twitter.com/darktama"><i class="fa fa-twitter fa-fw"></i></a>
            <a href="https://facebook.com/darktama"><i class="fa fa-facebook fa-fw"></i></a>
            <a href="#"><i class="fa fa-linkedin fa-fw"></i></a>
            <a href="https://plus.google.com/+EkatamaIlhamPrayoga"><i class="fa fa-google-plus fa-fw"></i></a></li> --}}
          </ul>
        </div>
      </div>

      <a href="#top" class="scrolltop">Top</a>

    </div>
  </footer>