<div id="content">
    <!-- Mission Statement -->
    <div class="mission text-center block block-pd-sm block-bg-noise">
      @if(count($profiles)>0)
      @foreach($profiles->all() as $prof)
      <div class="container">
        <h2 class="text-shadow-white">
            {{$prof->slogan}}
            <a href="about" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
          </h2>
      </div>
      @endforeach
      @endif
    </div>
    <!--Showcase-->
    <div class="showcase block block-border-bottom-grey">
      <div class="container">
        <h2 class="block-title">
            Showcase
          </h2>
        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a
          sit amet mauris.</p>
        <div class="item-carousel" data-toggle="owlcarousel" data-owlcarousel-settings='{"items":4, "pagination":false, "navigation":true, "itemsScaleUp":true}'>
          @if(count($produk)>0)
          {{-- shuffle($produk); --}}
        @foreach($produk->take(4) as $tam)
          <div class="item">
            <a href="{{$tam->lokasi}}" value="{{$tam->id_prod}}" data-target="#picModal" class="overlay-wrapper">
                <img id="pic" src="{{$tam->lokasi}}" alt="{{$tam->deskripsi}}" class="img-responsive underlay">
                <span class="overlay">
                  <span class="overlay-content"> <span class="h4">{{$tam->produk}}</span> </span>
                </span>
              </a>
            <div class="item-details bg-noise">
              <h4 class="item-title">
                  <a href="#">{{$tam->deskripsi}}</a>
                </h4>
              <a href="#" class="btn btn-more"><i class="fa fa-plus"></i>Read more</a>
            </div>
          </div>
         @endforeach
        @endif 
        </div>
        
      </div>
    </div>
    <!-- Services -->
    <div class="services block block-bg-gradient block-border-bottom">
      <div class="container">
        <h2 class="block-title">
            Our Services
          </h2>
        <div class="row">
          @if(count($services)>0)
          @foreach($services->all() as $serv)
          <div class="col-md-4 text-center">
            <span class="fa-stack fa-5x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa {{$serv->ikon}} fa-stack-1x fa-inverse"></i> </span>
            <h4 class="text-weight-strong">
                {{$serv->servis}}
              </h4>
            <p>{{$serv->deskripsi}}</p>
            <p>
              <a href="#" class="btn btn-more i-right">Learn More <i class="fa fa-angle-right"></i></a>
            </p>
          </div>
          @endforeach
          @endif
        </div>
      </div>
    </div>
    <!-- Pricing -->
    <div class="block-contained">
      <h2 class="block-title">
          Our Plans
        </h2>
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default panel-pricing text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Flex<em>Starter</em>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">19.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>3 User Accounts</li>
                <li>3 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>5GB of space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel panel-default panel-pricing panel-pricing-highlighted text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Team<em>Starter</em>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">49.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>50 User Accounts</li>
                <li>50 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3 text-center">
          <div class="panel panel-default panel-pricing panel-pricing-highlighted text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Enterprise
                  <span class="panel-pricing-popular"><i class="fa fa-thumbs-up"></i> Popular</span>
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">199.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>100 User Accounts</li>
                <li>100 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
        <div class="col-md-3 text-center">
          <div class="panel panel-default panel-pricing text-center">
            <div class="panel-heading">
              <h4 class="panel-title">
                  Corporate
                </h4>
            </div>
            <div class="panel-pricing-price">$ <span class="digits">299.95</span> /mo.</div>
            <div class="panel-body">
              <ul class="list-dotted">
                <li>1000 User Accounts</li>
                <li>1000 Private Projects</li>
                <li>Umlimited Projects</li>
                <li>Unlimited space</li>
              </ul>
              <a href="#" class="btn btn-primary btn-sm">Choose Plan</a>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!--
Background image callout with CSS overlay
Usage: data-block-bg-img="IMAGE-URL" to apply a background image clearly via jQuery .block-bg-overlay = overlays the background image, colour is inherited from block-bg-* classes .block-bg-overlay-NUMBER = determines opcacity value of overlay from 1-9 (default is 5) ie. .block-bg-overlay-2 or .block-bg-overlay-6
-->
    <div class="block block-pd-sm block-bg-grey-dark block-bg-overlay block-bg-overlay-6 text-center" data-block-bg-img="https://picjumbo.imgix.net/HNCK1088.jpg?q=40&amp;w=1650&amp;sharp=30" data-stellar-background-ratio="0.3">
      <h2 class="h-xlg h1 m-a-0">
          <span data-counter-up>100,000,0</span>s
        </h2>
      <h3 class="h-lg m-t-0 m-b-lg">
          Of Happy Customers!
        </h3>
      <p>
        <a href="#" class="btn btn-more btn-lg i-right">Join them today! <i class="fa fa-angle-right"></i></a>
      </p>
    </div>
    <!--Customer testimonial & Latest Blog posts-->
    <div class="testimonials block-contained">
      <div class="row">
        <!--Customer testimonial-->
        @if(count($testi)>0)
        @foreach($testi->all() as $tes)
        <div class="col-md-6 m-b-lg">
          <h3 class="block-title">
              Testimonials
            </h3>
          <blockquote>
            <p>{{$tes->testimoni}}</p>
            {{-- <p>Our productivity &amp; sales are up! Customers are happy &amp; we couldn't be happier with this product!</p> --}}
            <img src="{{$tes->lokasi}}" alt="Charles Spencer Chaplin">
            <small>
                <strong>{{$tes->id_klien}}</strong>
                <br>
                British comic actor
              </small>
          </blockquote>
        </div>
        @endforeach
        @endif
        <!--Latest Blog posts-->
        <div class="col-md-6 blog-roll">
          <h3 class="block-title">
              Latest From Our Blog
            </h3>
          <!-- Blog post 1-->
          @if(count($blog)>0)
          @foreach($blog->all() as $bl)
          <div class="media">
            <div class="media-left hidden-xs">
              <!-- Date desktop -->
              <div class="date-wrapper"> <span class="date-m">Feb</span> <span class="date-d">01</span> </div>
            </div>
            <div class="media-body">
              <h4 class="media-heading">
                  <a href="#" class="text-weight-strong">{{$bl->judul}}</a>
                </h4>
              <!-- Meta details mobile -->
              <ul class="list-inline meta text-muted visible-xs">
                <li><i class="fa fa-calendar"></i> <span class="visible-md">Created:</span> Fri 1st Feb 2013</li>
                <li><i class="fa fa-user"></i> <a href="#">Joe</a></li>
              </ul>
              <p>
                {{$bl->isi_blog}}<br>
                <a href="blog/{{$bl->id}}">Read more <i class="fa fa-angle-right"></i></a>
              </p>
            </div>
          </div>
          @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
  <!-- /content -->
  <!-- Call out block -->
  
  </div>

<!-- Modal -->
{{-- <div id="picModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <img src="{{asset('gambar/'.$tam->gambar)}}" alt="{{$tam->gambar}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> --}}