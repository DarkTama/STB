<!-- Call out block -->
  <div class="block block-pd-sm block-bg-primary">
    <div class="container">
      <div class="row">
        <h3 class="col-md-4">
            Some of our Clients
          </h3>
        <div class="col-md-8">
          <div class="row">
            <!--Client logos should be within a 120px wide by 60px height image canvas-->
            @if(count($klien)>0)
            @foreach($klien->take(6) as $kln)
            <div class="col-xs-6 col-md-2">
              <a href="#" title="{{$kln->klien}}">
                  <img src="/liravel/public/{{$kln->lokasi}}" alt="{{$kln->klien}} logo" class="img-responsive">
                </a>
            </div>
            @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>