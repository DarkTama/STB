<div class="hero" id="highlighted">
      <div class="inner">
        <!--Slideshow-->
        <div id="highlighted-slider" class="container">
          <div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>
            <!--Slideshow content-->
            <!--Slide 1-->
            @if(count($slider)>0)
        @foreach($slider as $sli)
        @if($sli->aktif == 1)
            <div class="item">
              <div class="row">
                <div class="col-md-6 {{$sli->posisi}} item-caption">
                  <h2 class="h1 text-weight-light">
                      {{$sli->judulsld}}
                    </h2>
                  <h4>
                      {{$sli->subjudul}}
                    </h4>
                  <p>{{$sli->prakata}}</p>
                  {{-- <a href="https://bootstrapmade.com" class="btn btn-more btn-lg i-right">Buy Now <i class="fa fa-plus"></i></a> --}}
                </div>
                <div class="{{$sli->posisigambar}}">
                  <img src="{{$sli->lokasi}}" class="fit img-responsive underlay">
                  {{-- <img src="img/slides/slide1.png" alt="Slide 1" class="center-block img-responsive"> --}}
                </div>
              </div>
            </div>
            @else
            @endif
            @endforeach
            @endif
            <!--Slide 2-->
            {{-- <div class="item">
              <div class="row">
                <div class="col-md-6 text-right-md item-caption">
                  <h2 class="h1 text-weight-light">
                      <span class="text-primary">Flexor</span> Bootstrap Theme
                    </h2>
                  <h4>
                      High quality, responsive theme!
                    </h4>
                  <p>Perfect for your App, Web service, company or portfolio! Magna tincidunt sociis ac integer amet non. Rhoncus augue? Tempor porttitor sed, aliquet phasellus a, nisi nunc aliquet nec rhoncus enim porttitor ultrices lacus tristique?</p>
                  <a href="https://bootstrapmade.com" class="btn btn-more btn-lg"><i class="fa fa-plus"></i> Learn More</a>
                </div>
                <div class="col-md-6 hidden-xs">
                  <img src="img/slides/slide2.png" alt="Slide 2" class="center-block img-responsive">
                </div>
              </div>
            </div> --}}
          </div>
        </div>
      </div>
    </div>