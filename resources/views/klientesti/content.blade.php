<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row row-eq-height">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Klien</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelKlien" class="table table-stripped" width="100%">
                    <thead>
                      <th width="5%"><button data-toggle="modal" data-target="#tambahKlien" class="btn btn-default btn-sm tambahKlien"><i class="fa fa-plus"></i></button></th>
                      <th width="6%">No</th>
                      <th width="20%">Klien</th>
                      <th>Alamat</th>
                      <th>Telp</th>
                      <th>Email</th>
                      <th width="5%">Logo</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row row-eq-height">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Testimoni</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelTesti" class="table table-stripped" width="100%">
                    <thead>
                      <th width="5%"><button data-toggle="modal" data-target="#tambahTesti" class="btn btn-default btn-sm tambahTesti"><i class="fa fa-plus"></i></button></th>
                      <th width="6%">No</th>
                      <th width="20%">Klien</th>
                      <th>Nama</th>
                      <th width="25%">Testimoni</th>
                      <th width="5%">Foto</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </section>
  </div>
