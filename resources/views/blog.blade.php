<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>About - Flexor Bootstrap Theme</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Fav and touch icons -->
  <link rel="shortcut icon" href="img/icons/favicon.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
  <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/owl.theme.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/owl.transitions.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">

  <!--Your custom colour override - predefined colours are: colour-blue.css, colour-green.css, colour-lavander.css, orange is default-->
  <link href="#" id="colour-scheme" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Flexor
    Theme URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->

</head>

<!-- ======== {{-- @Region --}}: body ======== -->

<body class="page-about">
  <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
  <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.8">

    <!-- ======== {{-- @Region --}}: #navigation ======== -->
    <div id="navigation" class="wrapper">
      <!--Hidden Header Region-->
      <div class="header-hidden collapse">
        <div class="header-hidden-inner container">
          <div class="row">
            <div class="col-md-3">
              <h3>
                  About Us
                </h3>
              <p>Flexor is a super flexible responsive theme with a modest design touch.</p>
              <a href="about" class="btn btn-more"><i class="fa fa-plus"></i> Learn more</a>
            </div>
            <div class="col-md-3">
              <!--{{-- @todo --}}: replace with company contact details-->
              <h3>
                  Contact Us
                </h3>
              <address>
                  <strong>Flexor Bootstrap Theme Inc</strong>
                  <abbr title="Address"><i class="fa fa-pushpin"></i></abbr>
                  Sunshine House, Sunville. SUN12 8LU.
                  <br>
                  <abbr title="Phone"><i class="fa fa-phone"></i></abbr>
                  019223 8092344
                  <br>
                  <abbr title="Email"><i class="fa fa-envelope-alt"></i></abbr>
                  info@flexorinc.com
                </address>
            </div>
            <div class="col-md-6">
              <!--Colour & Background Switch for demo - {{-- @todo --}}: remove in production-->
              <h3>
                  Theme Variations
                </h3>
              <div class="switcher">
                <div class="cols">
                  Backgrounds:
                  <br>
                  <a href="#benches" class="background benches active" title="Benches">Benches</a> <a href="#boots" class="background boots " title="Boots">Boots</a> <a href="#buildings" class="background buildings " title="Buildings">Buildings</a>
                  <a
                    href="#city" class="background city " title="City">City</a> <a href="#metro" class="background metro " title="Metro">Metro</a>
                </div>
                <div class="cols">
                  Colours:
                  <br>
                  <a href="#orange" class="colour orange active" title="Orange">Orange</a> <a href="#green" class="colour green " title="Green">Green</a> <a href="#blue" class="colour blue " title="Blue">Blue</a> <a href="#lavender" class="colour lavender "
                    title="Lavender">Lavender</a>
                </div>
              </div>
              <p>
                <small>Selection is not persistent.</small>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!--Header & navbar-branding region-->
      @include('flexor.header')
      @include('flexor.container')
    </div>
  </div>

  <!-- ======== {{-- @Region --}}: #content ======== -->
  <div id="content">
    <div class="container" id="about">
      <div class="row">
        <!--main content-->
        <div class="col-md-9 col-md-push-3">
          <div class="page-header">
            <h1>
                About Us
                <small>How it all began</small>
              </h1>
          </div>
          <div class="block block-border-bottom-grey block-pd-sm">
            <h3 class="block-title">
                {{$blog->judul}}
              </h3>
            {{-- @if(count($profiles)>0)
            @foreach($profiles->where('id_prof', 1) as $prof) --}}
            {{-- <img src="{{$prof->lokasi}}" alt="About us" class="img-responsive img-thumbnail pull-right m-l m-b"> --}}
            {{-- {{$prof->profil}} --}}{{$blog->isi_blog}}
            {{-- @endforeach
            @endif --}}
          </div>
          <div class="block-highlight block-pd-h block-pd-sm">
            <h3 class="block-title">
                Our Mission
              </h3>
            <p class="text-fancy">Vel in amet mauris? Turpis pulvinar a proin tincidunt pid, vel odio lundium sit, cras. Duis mattis porttitor nunc elementum eros, in turpis nec. Lectus dictumst risus ut ac porta, urna ac vel, a ut dis!</p>
          </div>
          <div class="block">
            <h3 class="block-title">
                Vital Stats
              </h3>
            <div class="row">
              <div class="col-md-4">
                <div class="stat">
                  <span data-counter-up>1000</span>s
                  <small>Happpy Customers</small>
                </div>
              </div>
              <div class="col-md-4">
                <div class="stat">
                  <span data-counter-up>163</span>+
                  <small>GB Transfered</small>
                </div>
              </div>
              <div class="col-md-4">
                <div class="stat">
                  <span data-counter-up>214</span>
                  <small>Bugs Fixed</small>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- sidebar -->
        <div class="col-md-3 col-md-pull-9 sidebar visible-md-block visible-lg-block">
          <ul class="nav nav-pills nav-stacked">
            @if(count($blogs)>0)
            @foreach($blogs->all() as $bl)
            <li class="">
              <a href="{{$bl->id_blog}}" class="first">
                  {{$bl->judul}}
                  <small>{{$bl->namauser}}</small>
                </a>
            </li>
            {{-- <li>
              <a href="#">
                  The Team
                  <small>Our team of stars</small>
                </a>
            </li>
            <li>
              <a href="#">
                  Contact Us
                  <small>How to get in touch</small>
                </a>
            </li> --}}
            @endforeach
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Call out block -->
  <div class="block block-bg-primary block-bg-overlay block-bg-overlay-5 text-center" data-block-bg-img="https://picjumbo.imgix.net/HNCK8991.jpg?q=40&amp;w=1650&amp;sharp=30" data-stellar-background-ratio="0.3">
    <h2 class="m-t-0">
        Be part of our amazing team!
      </h2>
    <p class="m-a-0">
      <a href="#" class="btn btn-more btn-lg i-right">We're Hiring <i class="fa fa-angle-right"></i></a>
    </p>
  </div>
  <!-- Call out block -->
  @include('flexor.clients')

  <!-- ======== {{-- @Region --}}: #footer ======== -->
  @include('flexor.footer')

  <!-- Required JavaScript Libraries -->
  <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('lib/stellar/stellar.min.js')}}"></script>
  <script src="{{asset('lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('lib/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('contactform/contactform.js')}}"></script>

  <!-- Template Specisifc Custom Javascript File -->
  <script src="{{asset('js/custom.js')}}"></script>

  <!--Custom scripts demo background & colour switcher - OPTIONAL -->
  <script src="{{asset('js/color-switcher.js')}}"></script>

  <!--Contactform script -->
  <script src="{{asset('contactform/contactform.js')}}"></script>

</body>

</html>
