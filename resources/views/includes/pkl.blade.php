{{-- {{$pekaels->all() = $pkl}} --}}
<table class="table table-striped"  cellspacing="0" width="100%" id="tabel" align="center">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nama Siswa</th>
				<th>Kontak</th>
				<th width="30%">Alamat</th>
				<th></th>
			</tr>
		</thead>
		{{-- <tbody id="datan">
			@if(count($pekaels)>0)
			@foreach($pekaels->all() as $sat)
			<tr class="siswa{{$sat->id}}">
				<td id="idi">{{$sat->id}}</td>
				<td>{{$sat->nama}}</td>
				<td>{{$sat->kelas}}</td>
				<td>{{$sat->lokasi}}</td>
				<td>
					<button class="btn btn-warning btn-sm edit" data-id="{{$sat->id}}" data-nama="{{$sat->nama}}" data-kelas="{{$sat->kelas}}" data-lokasi="{{$sat->lokasi}}">Edit</button>
					<button class="btn btn-danger btn-sm delete" data-id="{{$sat->id}}" data-nama="{{$sat->nama}}" data-kelas="{{$sat->kelas}}" data-lokasi="{{$sat->lokasi}}">Hapus</button>
				</td>
			</tr>
			@endforeach
			@endif
		</tbody> --}}
	</table>
	<div align="center" class="container">
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Input Data</h4>
        </div>
        <div class="modal-body">
        	{{-- <div class="container">
        		<div class="row">
        			<div class="col-lg">
        				<div class="alert alert-info info" style="display: none">
        					<ul></ul>
        				</div>
        			</div>
        		</div>
        	</div> --}}
        @if ($errors->any())
			<div class="badge badge-warning" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				</em>
			</div>
		@endif	
        <form class="form-horizontal" id="formulir">
		{{csrf_field()}}
		<table>
			<tr id="namae">
				<td>
					<label for="nama">Nama Lengkap</label>
				</td>
				<td>
					<input class="form-control" type="text" id="nama_tambah" name="nama" placeholder="Nama Lengkap" required>
					<div class="badge badge-warning" id="nama_error" hidden></div>
				</td>
			</tr>
			<tr id="kelase">
				<td>
					<label for="kelas">Kontak</label>
				</td>
				<td>
					<input class="form-control" type="text" id="kelas_tambah" name="kelas" placeholder="X-TKI 1" required="">
					<div class="badge badge-warning" id="kelas_error" hidden></div>
				</td>
			</tr>
			<tr id="lokasie">
				<td>
					<label for="lokasi">Alamat</label>
				</td>
				<td>
					<input class="form-control" type="text" id="lokasi_tambah" name="lokasi" placeholder="PT. Robot Maid" required>
					<div class="badge badge-warning" id="lokasi_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">

				</td>
			</tr>
		</table>
		</form>
        </div>
        <div class="modal-footer">
        	<button id="tambah" class="btn btn-primary actionBtn">Masukkan</button>
         	<button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  	</div>
	</div>
<div align="center" class="container">
  <!-- Trigger the modal with a button -->
{{-- <button align="center" type="button" class="fa fa-plus my-float float" data-toggle="modal" id="adda">+</button> --}}
  <!-- Modal -->
  <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
        	@if ($errors->any())
			<div class="badge badge-warning" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				</em>
			</div>
		@endif	
        <form class="form-horizontal" id="formulir">
		{{csrf_field()}}
		<table>
			<tr>
				<td>
					<label class="control-label" for="id">ID</label>
				</td>
				<td>
					<input type="text" name="id" id="id_edit" value="" class="form-control" disabled>
					<div PUT id="id_edit_error" hidden>Isi</div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="nama">Nama Lengkap</label>
				</td>
				<td>
					<input class="form-control" type="text" id="nama_edit" name="nama" required>
					<div class="badge badge-warning" id="nama_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="kelas">Kontak</label>
				</td>
				<td>
					<input class="form-control" type="text" id="kelas_edit" name="kelas" required="">
					<div class="badge badge-warning" id="kelas_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="lokasi">Alamat</label>
				</td>
				<td>
					<input class="form-control" type="text" id="lokasi_edit" name="lokasi" required>
					<div class="badge badge-warning" id="lokasi_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">

				</td>
			</tr>
		</table>
		</form>
        </div>
        <div class="modal-footer">
        	<button id="edit" class="btn btn-primary edit" value="tambah">Ubah</button>
         	<button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  	</div>
	</div>
	<div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Hapus Data ini?</h4>
                </div>
                <div class="modal-body">
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control del" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Nama</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="nama_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Kontak</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="kelas_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Alamat</label>
                            <div class="col-sm-10">
															
                                <input type="text" class="form-control" id="lokasi_delete" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" >Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
