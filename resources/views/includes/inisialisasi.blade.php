<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row row-eq-height">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Jenis</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelJenis" class="table table-stripped" width="100%">
                    <thead>
                      <th width="10%"><button data-toggle="modal" data-target="#tambahJenis" class="btn btn-default btn-sm tambahJenis" id="tambahJeniss"><i class="fa fa-plus"></i></button></th>
                      <th width="10%">No</th>
                      <th>Jenis</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Link Video</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelVideo" class="table table-stripped" width="100%">
                    <thead>
                      <th width="10%">
                        <button type="button" class="btn btn-default btn-sm tambahVideo" data-toggle="modal" data-target="#tambahVideo"><i class="fa fa-plus"></i></button>
                      <th width="10%">No</th>
                      <th>Judul</th>
                      <th>Link</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Slider</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelSlider" class="table table-stripped" width="100%">
                    <thead>
                      <th width="5%"><button class="btn btn-default btn-sm tambahSlider" data-toggle="modal" data-target="#tambahSlider"><i class="fa fa-plus"></i></button></th>
                      <th width="5%">No</th>
                      <th>Judul</th>
                      <th>Subjudul</th>
                      <th>Prakata</th>
                      <th>Posisi</th>
                      <th>Urut</th>
                      <th>Aktif</th>
                      <th>Gambar</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </section>
  </div>
