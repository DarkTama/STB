{{-- <table class="table table-striped"  cellspacing="0" width="100%" id="tabled" align="center">
		<thead>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Author</th>
				<th width="30%">Content</th>
				<th></th>
			</tr>
		</thead>
	</table> --}}
	<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Postingan</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a data-toggle="modal" id="adda">Tambah Data</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped"  cellspacing="0" width="100%" id="tabled" align="center">
						<thead>
							<tr>
								<th>ID</th>
								<th>Title</th>
								{{-- <th>Content</th> --}}
								<th>Author</th>
								<th></th>
							</tr>
						</thead>
					</table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

	<div align="center" class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Input Data</h4>
        </div>
        <div class="modal-body">
        	{{-- <div class="container">
        		<div class="row">
        			<div class="col-lg">
        				<div class="alert alert-info info" style="display: none">
        					<ul></ul>
        				</div>
        			</div>
        		</div>
        	</div> --}}
       {{--  @if ($errors->any())
			<div class="badge badge-warning" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				</em>
			</div>
		@endif	 --}}
        <form class="form-horizontal" id="formulir">
		{{csrf_field()}}
		<table>
			<tr id="namae">
				<td>
					<label for="author">Author</label>
				</td>
				<td>
					<select class="form-control" id="nama_tambah" name="author">
						@if(count($pekaels)>0)
						@foreach($pekaels->all() as $pkl)
							<option value="{{$pkl->id}}">{{$pkl->nama}}</option>
						@endforeach
						@endif
					</select>
					{{-- <input class="form-control" type="text" id="nama_tambah" name="nama" placeholder="Author" required> --}}
					<div class="badge badge-warning" id="nama_error" hidden></div>
				</td>
			</tr>
			<tr id="kelase">
				<td>
					<label for="title">Title</label>
				</td>
				<td>
					<input class="form-control" type="text" id="kelas_tambah" name="title" placeholder="Title of your Posts" required="">
					<div class="badge badge-warning" id="kelas_error" hidden></div>
				</td>
			</tr>
			<tr id="lokasie">
				<td>
					<label for="content">Content</label>
				</td>
				<td>
					<textarea class="form-control" type="text" id="lokasi_tambah" name="content" placeholder="Content of your Posts" required></textarea>
					{{-- <input class="form-control" type="text" id="lokasi_tambah" name="lokasi" placeholder="PT. Robot Maid" required> --}}
					<div class="badge badge-warning" id="lokasi_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">

				</td>
			</tr>
		</table>
		</form>
        </div>
        <div class="modal-footer">
        	<button id="tambaha" class="btn btn-primary actionBtn">Masukkan</button>
         	<button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  	</div>
	</div>
<div align="center" class="container">
  <!-- Trigger the modal with a button -->
{{-- <button align="center" type="button" class="fa fa-plus my-float float" data-toggle="modal" id="adda">+</button> --}}
  <!-- Modal -->
  <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
        	@if ($errors->any())
			<div class="badge badge-warning" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				</em>
			</div>
		@endif	
        <form class="form-horizontal" id="formulir">
		{{csrf_field()}}
		<table>
			<tr>
				<td>
					<label class="control-label" for="id">ID</label>
				</td>
				<td>
					<input type="text" name="id" id="id_edit" value="" class="form-control" disabled>
					<div PUT id="id_edit_error" hidden>Isi</div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="nama">Author</label>
				</td>
				<td>
					{{-- <select class="form-control" id="nama_edit" name="author">
						@if(count($pekaels)>0)
						@foreach($pekaels->all() as $pkl)
							<option value="{{$pkl->id}}">{{$pkl->nama}}</option>
						@endforeach
						@endif
					</select> --}}
					<input class="form-control" type="text" id="nama_edit" name="nama" disabled required>
					<div class="badge badge-warning" id="nama_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="kelas">Title</label>
				</td>
				<td>
					<input class="form-control" type="text" id="kelas_edit" name="title" required="">
					<div class="badge badge-warning" id="kelas_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td>
					<label class="control-label" for="lokasi">Content</label>
				</td>
				<td>
					<textarea class="form-control" type="text" id="lokasi_edit" name="informasi" required></textarea>
					{{-- <input class="form-control" type="text" id="lokasi_edit" name="lokasi" required> --}}
					<div class="badge badge-warning" id="lokasi_edit_error" hidden></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">

				</td>
			</tr>
		</table>
		</form>
        </div>
        <div class="modal-footer">
        	<button id="edita" class="btn btn-primary edita" value="tambah">Ubah</button>
         	<button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
  	</div>
	</div>
	<div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Hapus Data ini?</h4>
                </div>
                <div class="modal-body">
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control del" id="id_delete" disabled>
                                {{-- <select class="form-control" id="nama_tambah" name="author">
									@if(count($pekaels)>0)
									@foreach($pekaels->all() as $pkl)
										<option value="{{$pkl->id}}">{{$pkl->nama}}</option>
									@endforeach
									@endif
								</select> --}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Nama</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="nama_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="kelas_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Content</label>
                            <div class="col-sm-10">
															
                                <input type="text" class="form-control" id="lokasi_delete" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger deletea" >Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>