<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Postingan
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i> Home</a></li>
        <li class="active">Postingan</li>
      </ol>
    </section>
    <section class="content">
      @include('includes.post')
        </div>
      </div>
    </section>
  </div>