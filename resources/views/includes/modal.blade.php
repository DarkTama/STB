<div id="editProf" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodal" width="100%">
      <tr hidden="">
        <div class="row">
        <td>
          <label class="control-label" for="idprof">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idprof" name="idprof" required disabled="">
        </td>
        </div>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="slogan">Slogan</label>
        </td>
        <td>
          <input class="form-control" type="text" id="slogan_edit" name="slogan" required>
          <div class="badge badge-warning" id="slogan_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="highlight">Highlight</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="highlight_edit" name="highlight" required=""></textarea>
          {{-- <input class="form-control" type="text" id="highlight_edit" name="highlight" required=""> --}}
          <div class="badge badge-warning" id="highlight_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="profil">Profil</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="profil_edit" name="profil" required></textarea>
          {{-- <input class="form-control" type="text" id="profil_edit" name="profil" required> --}}
          <div class="badge badge-warning" id="profil_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">

        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success edit" id="edit">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr id="idid" hidden="">
        <td>
          <label class="control-label" for="id"></label>
        </td>
        <td>
          <input class="form-control" type="text" id="id" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jenis" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Kontak</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="kontak" name="kontak">
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Ikon</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="ikon_edit" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="ikon" name="ikon" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Urut</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="urut" name="urut" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addKontak" id="addKontak">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="edi" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="ide" name="id" required disabled="">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jenis_edit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Kontak</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="kontak_edit" name="kontak">
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Ikon</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="ikon_edit" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="ikon_edit" name="ikon" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Urut</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="urut_edit" name="urut" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success ediKontak" id="ediKontak">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idids" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="del">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahSosmed" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jeniss" name="jeniss" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="sosmed">Sosmed</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="sosmed" name="sosmed">
          <div class="badge badge-warning" id="sosmed_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Ikon</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="ikon_edit" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="ikons" name="ikons" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Urut</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="uruts" name="uruts" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addSosmed" id="addSosmed">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediSosmed" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idsosmed" required="" disabled="">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jenisedit" name="jeniss" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="sosmed">Sosmed</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="sosmededits" name="sosmed">
          <div class="badge badge-warning" id="sosmed_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Ikon</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="ikon_edit" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="ikonedit" name="ikons" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Urut</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="urutedit" name="uruts" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success EditSosmed" id="EditSosmed">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusSosmed" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapus" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="dele">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahOffice" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodal" width="100%">
      <tr id="idid" hidden="">
        <td>
          <label class="control-label" for="id"></label>
        </td>
        <td>
          <input class="form-control" type="text" id="idoffice" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Nama Kantor</label>
        </td>
        <td>
          <input class="form-control" type="text" id="namaoffice" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Alamat</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="alamatoffice" name="kontak" required=""></textarea>
          {{-- <input class="form-control" type="text" id="alamatoffice" name="kontak"> --}}
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Kota</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kotaoffice" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="kotaoffice" name="ikon" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Kode POS</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="kodepos" name="urut" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">No Telephone</label>
        </td>
        <td>
          <input class="form-control" type="text" name="no" id="telephone">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Koordinat</label>
        </td>
        <td>
          <input class="form-control" type="text" name="koordinat" id="koordinat">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Aktif</label>
        </td>
        <td>
          {{-- <input class="form-control" type="text" id="aktifoffice"> --}}
          <input class="form-check-input" type="checkbox" id="aktifoffice" value="1">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addOffice" id="addOffices">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediOffice" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodal" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input disabled="" class="form-control" type="text" id="idofficeedit" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Nama Kantor</label>
        </td>
        <td>
          <input class="form-control" type="text" id="namaofficeedit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Alamat</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="alamatofficeedit" name="kontak" required=""></textarea>
          {{-- <input class="form-control" type="text" id="alamatoffice" name="kontak"> --}}
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="ikon">Kota</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kotaoffice" name="ikon" required></textarea> --}}
          <input class="form-control" type="text" id="kotaofficeedit" name="ikon" required>
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Kode POS</label>
        </td>
        <td>
          {{-- <textarea class="control-label" type="text"></textarea> --}}
          <input class="form-control" type="text" id="kodeposedit" name="urut" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">No Telephone</label>
        </td>
        <td>
          <input class="form-control" type="text" name="no" id="telephoneedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Koordinat</label>
        </td>
        <td>
          <input class="form-control" type="text" name="koordinat" id="koordinatedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Aktif</label>
        </td>
        <td>
          {{-- <input class="form-control" type="text" id="aktifofficeedit"> --}}
          <input class="form-check-input" type="checkbox" id="aktifofficeedit" value="1">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addOffice" id="editOffices">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusOffice" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusoffice" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="delet">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahVideo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr id="idid" hidden="">
        <td>
          <label class="control-label" for="id"></label>
        </td>
        <td>
          <input class="form-control" type="text" id="idvid" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul Video</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulvid" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Link Video</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="linkvid" name="kontak">
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addVideo" id="addVideo">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediVideo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idvidedit" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul Video</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulvidedit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Link Video</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="kontak_edit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="linkvidedit" name="kontak">
          <div class="badge badge-warning" id="kontak_error" hidden></div>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editVideo" id="editVideo">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusVideo" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idvidhapus" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleted">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahJenis" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr id="idid" hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idjps" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jenisjps" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="addJenis">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediJenis" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idjpsedit" name="id" required>
          
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <input class="form-control" type="text" id="jenisjpsedit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="editJenise">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusJenis" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idjpshapus" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteJenis">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahSlider" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodal" width="100%">
      {{-- <tr id="idid" hidden="">
        <td>
          <label class="control-label" for="id"></label>
        </td>
        <td>
          <input class="form-control" type="text" id="idd" name="id" required>
        </td>
      </tr> --}}
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulsld" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Subjudul</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="subjudulsld" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="subjudulsld" name="kontak">
                <div class="badge badge-warning" id="kontak_error" hidden></div>
              </td>
            </tr>
            <tr>
              <td>
          <label class="control-label" for="ikon">Prakata</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="prakatasld" name="ikon" required></textarea>
          {{-- <input class="form-control" type="text" id="prakatasld" name="ikon" required> --}}
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea class="form-control" id="deskripsisld"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Posisi</label>
        </td>
        <td>
          ​<label class="radio-inline">
            <input type="radio" id="kirisld" name="posisisld" value="col-md-6 hidden-xs:text-right-md">Kiri
          </label>
          <label class="radio-inline">
            <input type="radio" id="kanansld" name="posisisld" value="col-md-6 col-md-pull-6 hidden-xs:col-md-push-6">Kanan
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Urut</label>
        </td>
        <td>
          <input class="form-control" type="text" name="no" id="urutsld">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Aktif</label>
        </td>
        <td>
          <input type="checkbox" id="aktifsld" value="1">
        </td>
        {{-- <td>
          <input class="form-control" type="text" id="aktifoffice">
          <input class="form-check-input" type="checkbox" id="aktifoffice">
        </td> --}}
      </tr>
      {{-- <tr>
        <td>
          <label>Unggah Foto</label>
        </td>
        <td>
          <input type="file" name="picturesld">
        </td>
      </tr> --}}
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addSlider" id="addSlider">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediSlider" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodal" width="100%">
      <tr hidden="">
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input class="form-control" type="text" id="idsldedit" name="id" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulsldedit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kontak">Subjudul</label>
        </td>
        <td>
          {{-- <textarea class="form-control" type="text" id="subjudulsldedit" name="kontak" required=""></textarea> --}}
          <input class="form-control" type="text" id="subjudulsldedit" name="kontak">
                <div class="badge badge-warning" id="kontak_error" hidden></div>
              </td>
            </tr>
            <tr>
              <td>
          <label class="control-label" for="ikon">Prakata</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="prakatasldedit" name="ikon" required></textarea>
          {{-- <input class="form-control" type="text" id="prakatasldedit" name="ikon" required> --}}
          <div class="badge badge-warning" id="ikon_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea class="form-control" id="deskripsisldedit"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="urut">Posisi</label>
        </td>
        <td>
          ​<label class="radio-inline">
            <input type="radio" id="kirisld" name="posisisldedit" value="col-md-6 hidden-xs:text-right-md">Kiri
          </label>
          <label class="radio-inline">
            <input type="radio" id="kanansld" name="posisisldedit" value="col-md-6 col-md-pull-6 hidden-xs:col-md-push-6">Kanan
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Urut</label>
        </td>
        <td>
          <input class="form-control" type="text" name="no" id="urutsldedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Aktif</label>
        </td>
        <td>
          <input type="checkbox" id="aktifsldedit" value="1">
        </td>
        {{-- <td>
          <input class="form-control" type="text" id="aktifoffice">
          <input class="form-check-input" type="checkbox" id="aktifoffice">
        </td> --}}
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editSlider" id="editSlider">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusSlider" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapussld" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deletesld">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="pictureShow" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <img id="picture" src=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicSlider" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpicturesld')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="picturesldid" id="picturesldid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="picturesldup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="tambahProduks" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisProduk" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Produk</label>
        </td>
        <td>
          <input type="text" id="namaproduk" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiproduk" class="form-control"></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="addProduk">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediProduk" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td><input type="text" id="idProduk"></td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisProdukedit" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Produk</label>
        </td>
        <td>
          <input type="text" id="namaprodukedit" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiprodukedit" class="form-control"></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="editProduk">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusProduk" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusproduk" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteproduk">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicProduk" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureproduk')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureprodukid" id="pictureprodukid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureprodukup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

@if(count($produk)>0)
@foreach($produk as $prd)
<div id="pictureShows-{{$prd->id_prod}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      {{-- <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID"> --}}
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID">
    </form>
      @if(count($gambar)>0)
        @foreach($gambar->where('id_prod', $prd->id_prod) as $gb)
        {{-- <span>{{$gb->id}} --}}
          @if($gb->lokasi == null)
          <h3 align="center">Belum ada foto</h3>
          @else
          <img id="pictures" src="/liravel/public/{{$gb->lokasi}}">
          @endif
        {{-- </span> --}}
        @endforeach
      @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif

<div id="uploadpicProduks" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureproduks')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureprodukids" id="pictureprodukids">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureprodukups[]" id="imgInp" multiple>
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="hapusProdukGambar" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus Gambar</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusprodukgambar" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteprodukgambar">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahProjek" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisProjek" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Projek</label>
        </td>
        <td>
          <input type="text" id="namaprojek" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiprojek" class="form-control"></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="addProjek">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediProjek" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td><input type="text" id="idProjek"></td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisProjekedit" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Projek</label>
        </td>
        <td>
          <input type="text" id="namaprojekedit" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiprojekedit" class="form-control"></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="editProjek">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusProjek" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusprojek" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteprojek">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicProjek" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureprojek')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureprojekid" id="pictureprojekid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureprojekup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

@if(count($projek)>0)
@foreach($projek as $pj)
<div id="pictureProjek-{{$pj->id_prjt}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      {{-- <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID"> --}}
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID">
    </form>
      @if(count($gambar)>0)
        @foreach($gambar->where('id_prjt', $pj->id_prjt) as $gb)
        {{-- <span>{{$gb->id}} --}}
          {{-- @if($gb->lokasi != null) --}}
          <img id="pictures" src="/liravel/public/{{$gb->lokasi}}">
          {{-- @else
          <h3 align="center">Belum ada foto</h3>
          @endif --}}
        {{-- </span> --}}
        @endforeach
      @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif

<div id="tambahServis" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisServis" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Servis</label>
        </td>
        <td>
          <input type="text" id="namaservis" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiservis" class="form-control"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Ikon</label>
        </td>
        <td>
          <input type="text" id="ikonservis" class="form-control">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="addServis">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediServis" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr hidden="">
        <td><input type="text" id="idServis"></td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisServisedit" name="jenisProduk">
            @if(count($jenis)>0)
            @foreach($jenis->all() as $jen)
            <option value="{{$jen->id_jps}}">{{$jen->jps}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Servis</label>
        </td>
        <td>
          <input type="text" id="namaservisedit" class="form-control">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Deskripsi</label>
        </td>
        <td>
          <textarea type="text" id="deskripsiservisedit" class="form-control"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Ikon</label>
        </td>
        <td>
          <input type="text" id="ikonservisedit" class="form-control">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editJenis" id="editServis">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusServis" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusservis" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteservis">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahBlog" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Penulis</label>
        </td>
        <td>
          <select class="form-control" id="penulisblog">
            @if(count($users)>0)
            @foreach($users as $user)
            <option value="{{$user->id_usr}}">{{$user->namauser}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulblog" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Isi</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="isiblog" required></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success addBlog" id="addBlog">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediBlog" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <input type="text" id="idblog" hidden>
      <tr>
        <td>
          <label class="control-label" for="jenis">Penulis</label>
        </td>
        <td>
          <select class="form-control" id="penulisblogedit">
            @if(count($users)>0)
            @foreach($users as $user)
            <option value="{{$user->id_usr}}">{{$user->namauser}}</option>
            @endforeach
            @endif
          </select>
          {{-- <input class="form-control" type="text" id="jenisjps" name="jenis" required> --}}
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulblogedit" name="jenis" required>
          <div class="badge badge-warning" id="jenis_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Isi</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="isiblogedit" required></textarea>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success editBlog" id="editBlog">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusBlog" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusblog" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteblog">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicBlog" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureblog')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureblogid" id="pictureblogid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureblogup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

@if(count($blog)>0)
@foreach($blog as $bl)
<div id="pictureBlogs-{{$bl->id_blog}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      {{-- <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID"> --}}
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <input type="text" hidden="" id="pictureShowsID" name="pictureShowsID">
    </form>
      @if(count($gambar)>0)
        @foreach($gambar->where('id_blog', $bl->id_blog) as $gb)
        {{-- <span>{{$gb->id}} --}}
          {{-- @if($gb->lokasi != null) --}}
          <img id="pictures" src="/liravel/public/{{$gb->lokasi}}">
          {{-- @else
          <h3 align="center">Belum ada foto</h3>
          @endif --}}
        {{-- </span> --}}
        @endforeach
      @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif

<div id="tambahKlien" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Klien</label>
        </td>
        <td>
          <input class="form-control" type="text" id="klien" name="jenis" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Alamat</label>
        </td>
        <td>
          <textarea class="form-control" id="alamatklien" required></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Telepon</label>
        </td>
        <td>
          <input class="form-control" type="text" id="teleponklien" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Email</label>
        </td>
        <td>
          <input type="text" class="form-control" id="emailklien" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addKlien">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediKlien" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <input hidden type="text" id="idklien">
        <td>
          <label class="control-label" for="jenis">Klien</label>
        </td>
        <td>
          <input class="form-control" type="text" id="klienedit" name="jenis" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Alamat</label>
        </td>
        <td>
          <textarea class="form-control" id="alamatklienedit" required></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Telepon</label>
        </td>
        <td>
          <input class="form-control" type="text" id="teleponklienedit" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Email</label>
        </td>
        <td>
          <input type="text" class="form-control" id="emailklienedit" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="editKlien">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusKlien" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusklien" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteklien">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicKlien" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureklien')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureklienid" id="pictureklienid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureklienup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="tambahTesti" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Klien</label>
        </td>
        <td>
          <select class="form-control" id="klientesti">
            @if(count($klien)>0)
            @foreach($klien as $kl)
            <option value="{{$kl->id_klien}}">{{$kl->klien}}</option>
            @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Nama</label>
        </td>
        <td>
          <input class="form-control" id="namatesti" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Testimoni</label>
        </td>
        <td>
          <input class="form-control" type="text" id="testimoni" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addTesti">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediTesti" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <input type="text" id="idtesti" hidden>
          <label class="control-label" for="jenis">Klien</label>
        </td>
        <td>
          <select class="form-control" id="klientestiedit">
            @if(count($klien)>0)
            @foreach($klien as $kl)
            <option value="{{$kl->id_klien}}">{{$kl->klien}}</option>
            @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Nama</label>
        </td>
        <td>
          <input class="form-control" id="namatestiedit" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Testimoni</label>
        </td>
        <td>
          <input class="form-control" type="text" id="testimoniedit" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="editTesti">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusTesti" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td ><input class="form-control" type="text" id="idhapustesti" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deletetesti">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicTesti" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpicturetesti')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="picturetestiid" id="picturetestiid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="picturetestiup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="tambahPartner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Partner</label>
        </td>
        <td>
          <input class="form-control" type="text" id="partner">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Alamat</label>
        </td>
        <td>
          <input class="form-control" id="alamatpartner" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Telepon</label>
        </td>
        <td>
          <input class="form-control" type="text" id="teleponpartner" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Email</label>
        </td>
        <td>
          <input class="form-control" type="text" id="emailpartner">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addPartner">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediPartner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <input type="text" id="idpartner" hidden>
          <label class="control-label" for="jenis">Partner</label>
        </td>
        <td>
          <input class="form-control" type="text" id="partneredit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Alamat</label>
        </td>
        <td>
          <input class="form-control" id="alamatpartneredit" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Telepon</label>
        </td>
        <td>
          <input class="form-control" type="text" id="teleponpartneredit" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Email</label>
        </td>
        <td>
          <input class="form-control" type="text" id="emailpartneredit">
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="editPartner">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusPartner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapuspartner" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deletepartner">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicPartner" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpicturepartner')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="picturepartnerid" id="picturepartnerid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="picturepartnerup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="tambahUnduhan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">Judul Unduhan</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulunduhan">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Link Unduhan</label>
        </td>
        <td>
          <input class="form-control" id="linkunduhan" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addUnduhan">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediUnduhan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <input type="text" id="idunduhan" hidden>
          <label class="control-label" for="jenis">Judul Unduhan</label>
        </td>
        <td>
          <input class="form-control" type="text" id="judulunduhanedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Link Unduhan</label>
        </td>
        <td>
          <input class="form-control" id="linkunduhanedit" required>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="editUnduhan">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusUnduhan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapusunduhan" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteunduhan">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="tambahUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <label class="control-label" for="jenis">E-mail</label>
        </td>
        <td>
          <input class="form-control" type="text" id="emailusers">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Nama</label>
        </td>
        <td>
          <input class="form-control" id="namausers" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Status</label>
        </td>
        <td>
          <input type="checkbox" id="statususers" value="1">
          {{-- <input class="form-control" id="namausers" required> --}}
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisusers">
            @if(count($jenisuser)>0)
              @foreach($jenisuser as $user)
                <option value="{{$user->id_jns}}">{{$user->jenisuser}}</option>
              @endforeach
            @endif
          </select>
          {{-- <input type="text" name=""> --}}
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addUsers">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="ediUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table class="tablemodalsm" width="100%">
      <tr>
        <td>
          <input type="text" id="idusers" hidden>
          <label class="control-label" for="jenis">E-mail</label>
        </td>
        <td>
          <input class="form-control" type="text" id="emailusersedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Nama</label>
        </td>
        <td>
          <input class="form-control" id="namausersedit" required>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Status</label>
        </td>
        <td>
          <input type="checkbox" value="1" id="statususersedit">
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label">Jenis</label>
        </td>
        <td>
          <select class="form-control" id="jenisusersedit">
            @if(count($jenisuser)>0)
              @foreach($jenisuser as $user)
                <option value="{{$user->id_jns}}">{{$user->jenisuser}}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
    </table>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="editUsers">Simpan</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="hapusUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete</h4>
      </div>
      <div class="modal-body">
        <table>
          <tr>
            <td align="center">Hapus data</td>
            <td hidden=""><input class="form-control" type="text" id="idhapususers" name="idi" disabled=""></td>
            <td>?</td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="deleteusers">Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div id="uploadpicUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{('uploadpictureusers')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="text" hidden="" name="pictureusersid" id="pictureusersid">
          {{-- <input type="file" name="picturesldup"> --}}
          <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-default btn-file">
                    Browse… <input type="file" name="pictureusersup" id="imgInp">
                </span>
            </span>
            <input type="text" class="form-control" disabled readonly>
          </div>
          <img id='img-upload'/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-file" >Unggah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
      </form>
    </div>
  </div>
</div>

