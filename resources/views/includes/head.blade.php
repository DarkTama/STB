<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  {{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
  {{-- <link rel="stylesheet" href="css/bootstrap.min.css"> --}}
  {{-- <link rel="stylesheet" href="{{asset('css/now-ui-kit.css')}}"> --}}
  <!-- Font Awesome -->
  {{-- <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}"> --}}
  <link rel="stylesheet" type="text/css" href="{{asset('font-awesome/css/font-awesome.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" href="{{asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"> --}}
  {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.bootstrap.min.css')}}"> --}}
  <link rel="stylesheet" type="text/css" href="{{asset('/css/toastr.min.css')}}">
  <style type="text/css">
  #profil_edit{
    height: 300px;
  }
  #highlight_edit, #prakatasld, #deskripsisld, #prakatasldedit, #deskripsisldedit, #isiblogedit, #isiblog{
    height: 60px;
  }
  textarea{
    height: 60px;
  }
  .ddd{
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    opacity: 0.9;
  }
  .tablemodal{
    height: 400px;
  }
  .tablemodalsm{
    height: 190px;
  }
  .ddd:hover{
    opacity: 1.0;
  }
  .dddd{
    position: fixed;
    width: 230px;
    /*width: 100%;*/
  }
  textarea{
    resize: none;
    /*height: 300px;*/
  }
  .row-eq-height {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload, #picture{
    width: 100%;
}
#pictures{
  width:40%;
  /*align-content: center;*/
}
/*    body{
  width: 100%;
  height: 100%;
}
.btn-group-sm .btn-fab{
  position: fixed !important;
  right: 29px;
}
.btn-group .btn-fab{
  position: fixed !important;
  right: 20px;
}
#main{
  bottom: 20px;
}
#mail{
  bottom: 80px
}
#sms{
  bottom: 125px
}
#autre{
  bottom: 170px
}*/
/**{padding:0;margin:0;}*/

/*body{
  font-family:Verdana, Geneva, sans-serif;
  font-size:18px;
  background-color:#CCC;
}*/
#lokasi_tambah{
  height: 200px;
}
/*.float{
  position:fixed;
  width:60px;
  height:60px;
  bottom:40px;
  right:40px;
  background-color:#0C9;
  color:#FFF;
  border-radius:50px;
  text-align:center;
  box-shadow: 2px 2px 3px #999;
}*/
/*#dataTables_paginate{
  align-content: right;
}*/
/*.my-float{
  margin-top:22px;
}*/
  </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

