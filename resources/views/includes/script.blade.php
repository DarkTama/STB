<!-- jQuery 3 -->
<script type="text/javascript" src="{{asset('/js/jquery-3.2.1.min.js')}}"></script>
{{-- <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script> --}}
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
{{-- <script src="js/core/bootstrap.min.js"></script> --}}
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap  -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS -->
{{-- <script src="{{asset('bower_components/Chart.js/Chart.js')}}"></script> --}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{asset('dist/js/pages/dashboard2.js')}}"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('dist/js/demo.js')}}"></script> --}}
<script type="text/javascript" src="{{asset('/readmore-js/readmore.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/toastr.min.js')}}"></script>
{{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
<script type="text/javascript" src="{{asset('/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/dataTables.bootstrap.min.js')}}"></script>
{{-- <script type="text/javascript" src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script> --}}
{{-- <script type="text/javascript" src="{{asset('/js/script.js')}}"></script> --}}
<script type="text/javascript" src="{{asset('/js/stb.js')}}"></script>
{{-- <script type="text/javascript" src="/readmore-js/readmore.js"></script> --}}
{{-- <script src="{{node_modules('/readmore-js/readmore.min.js')}}"></script> --}}

{{-- <script src="https://raw.githubusercontent.com/jedfoster/Readmore.js/master/readmore.js"></script> --}}
