  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Profil Perushaan</h3>

              <div class="box-tools pull-right">
                {{-- <div class="btn-group"> --}}
                  {{-- <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"></button> --}}
                    {{-- <i class="fa fa-wrench"></i></button> --}}
                    <button type="button" class="btn btn-box-tool editProf"><i class="fa fa-edit"></i></button>
                    {{-- <a data-toggle="modal" class="btn btn-default btn-sm editProf"></a> --}}
                  {{-- <ul class="dropdown-menu" role="menu">
                    <li><a data-toggle="modal" id="adda">Tambah Data</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul> --}}
                {{-- </div> --}}
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  @if(count($profiles)>0)
                  @foreach($profiles->all() as $prof)
                  <p data-id="{{$prof->id_prof}}" data-slogan="{{$prof->slogan}}" data-highlight="{{$prof->highlight}}" data-profil="{{$prof->profil}}" id="idi"><strong>Slogan</strong></p>
                  <p id="slogan">{{$prof->slogan}}</p>
                  <p><strong>Highlight</strong></p>
                  <p id="highlight">{{$prof->highlight}}</p>
                  <p><strong>Profil</strong></p>
                  <p class="profil" id="profil">
                    {{ str_limit(strip_tags($prof->profil), 500) }}
            {{-- @if (strlen(strip_tags($prof->profil)) > 500)
              ...
            @endif --}}</p>
                  {{-- @include('includes.pkl') --}}
                  @endforeach
                  @endif
                </div>
                <!-- /.col -->
                {{-- @include('includes.goalcompletion') --}}
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
      </div>
      <div class="row row-eq-height">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Kontak</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelKontak" class="table table-stripped" width="100%">
                    <thead>
                      <th><button data-toggle="modal" data-target="#tambah" class="btn btn-default btn-sm tambahKontak" id="tambahKontak"><i class="fa fa-plus"></i></button></th>
                      <th>No</th>
                      <th>Jenis</th>
                      <th>Kontak</th>
                      <th>Ikon</th>
                      <th>Urut</th>
                    </thead>
                    {{-- @if(count($kontak)>0)
                    @foreach($kontak->all() as $kon)
                    <tr>
                      <td align="center">
                        <div class="dropdown">
                          <li class="fa fa-wrench dropdown-toggle" type="button" data-toggle="dropdown"></li>
                          <ul class="dropdown-menu">
                            <li><a data-toggle="modal" id="EditKontak">Edit</a></li>
                            <li><a data-toggle="modal" id="HapusKontak">Hapus</a></li>
                          </ul>
                        </div>
                      </td>
                      <td>{{$kon->id_ktk}}</td>
                      <td>{{$kon->jnskontak}}</td>
                      <td>{{$kon->dtlkontak}}</td>
                      <td>{{$kon->ikon}}</td>
                      <td>{{$kon->urut}}</td>
                    </tr>
                    @endforeach
                    @endif --}}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Media Sosial</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelSosmed" class="table table-stripped" width="100%">
                    <thead>
                      <th>
                        <button type="button" class="btn btn-default btn-sm tambahSosmed" data-toggle="modal" data-target="#tambahSosmed"><i class="fa fa-plus"></i></button>
                        {{-- <a class="tambahSosmed btn btn-default btn-sm" data-toggle="modal" data-target="tambahSosmed" id="tambahSosmed">+</a></th> --}}
                      <th>No</th>
                      <th>Jenis</th>
                      <th>Sosial Media</th>
                      <th>Ikon</th>
                      <th>Urut</th>
                    </thead>
                    {{-- @if(count($sosmed)>0)
                    @foreach($sosmed->all() as $sos)
                    <tr>
                      <td align="center">
                        <div class="dropdown">
                          <li class="fa fa-wrench dropdown-toggle" type="button" data-toggle="dropdown"></li>
                          <ul class="dropdown-menu">
                            <li><a data-toggle="modal" id="EditSosmed">Edit</a></li>
                            <li><a data-toggle="modal" id="HapusSosmed">Hapus</a></li>
                          </ul>
                        </div>
                      </td>
                      <td>{{$sos->id_sosmed}}</td>
                      <td>{{$sos->jnssosmed}}</td>
                      <td>{{$sos->dtlsosmed}}</td>
                      <td>{{$sos->ikon}}</td>
                      <td>{{$sos->urut}}</td>
                    </tr>
                    @endforeach
                    @endif --}}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Alamat Kantor</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="tabelOffice" class="table table-stripped" width="100%">
                    <thead>
                      <th><button class="btn btn-default btn-sm tambahOffice" data-toggle="modal" data-target="#tambahOffice"><i class="fa fa-plus"></i></button></th>
                      <th>No</th>
                      <th>Nama Kantor</th>
                      <th>Alamat</th>
                      <th>Kota</th>
                      <th>Kode Pos</th>
                      <th>No Telp</th>
                      <th>Koordinat</th>
                      <th>Aktif</th>
                    </thead>
                    {{-- @if(count($office)>0)
                    @foreach($office->all() as $off)
                    <tr>
                      <td align="center">
                        <div class="dropdown">
                          <li class="fa fa-wrench dropdown-toggle" type="button" data-toggle="dropdown"></li>
                          <ul class="dropdown-menu">
                            <li><a data-toggle="modal" id="EditOffice">Edit</a></li>
                            <li><a data-toggle="modal" id="HapusOffice">Hapus</a></li>
                          </ul>
                        </div>
                      </td>
                      <td>{{$off->id_off}}</td>
                      <td>{{$off->nama}}</td>
                      <td>{{$off->alamat}}</td>
                      <td>{{$off->kota}}</td>
                      <td>{{$off->kodepos}}</td>
                      <td>{{$off->telp}}</td>
                      <td>{{$off->kordinat}}</td>
                      <td>{{$off->aktif}}</td>
                    </tr>
                    @endforeach
                    @endif --}}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </section>
  </div>
  {{-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> --}}

{{-- @include('includes.button') --}}
