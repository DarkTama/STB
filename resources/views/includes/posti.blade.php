
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Content
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-book"></i> Home</a></li>
        <li class="active">Content</li>
      </ol>
    </section>
    @if(count($pekaels)>0)
    @foreach($pekaels->all() as $pkl)
    <section class="content">
        <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 id="title" class="box-title"><b>{{$pkl->title}}</b></h3>
              {{-- {{$pkl->pekael->nama}} --}}
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a data-toggle="modal" class="edi" id="edi">Edit Data</a></li>
                  </ul>
                </div>
                {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <p id="content">{{$pkl->informasi}}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
      @endif
      </div>
        </div>
      </div>
    </section>

  </div>
<div align="center" class="container">
  <!-- Trigger the modal with a button -->
{{-- <button align="center" type="button" class="fa fa-plus my-float float" data-toggle="modal" id="adda">+</button> --}}
  <!-- Modal -->
  <div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">
          @if ($errors->any())
      <div class="badge badge-warning" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><em>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
        </em>
      </div>
    @endif  
        <form class="form-horizontal" id="formulir">
    {{csrf_field()}}
    <table>
      <tr>
        <td>
          <label class="control-label" for="id">ID</label>
        </td>
        <td>
          <input type="text" name="id" id="id_edit" value="{{$pkl->id}}" class="form-control" disabled>
          <div PUT id="id_edit_error" hidden>Isi</div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="nama">Author</label>
        </td>
        <td>
          {{-- <select class="form-control" id="nama_edit" name="author">
            @if(count($pekaels)>0)
            @foreach($pekaels->all() as $pkl)
              <option value="{{$pkl->id}}">{{$pkl->nama}}</option>
            @endforeach
            @endif
          </select> --}}
          <input class="form-control" type="text" id="nama_edit" name="nama" disabled required>
          <div class="badge badge-warning" id="nama_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="kelas">Title</label>
        </td>
        <td>
          <input class="form-control" type="text" id="kelas_edit" name="title" required="">
          <div class="badge badge-warning" id="kelas_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" for="lokasi">Content</label>
        </td>
        <td>
          <textarea class="form-control" type="text" id="lokasi_edit" name="content" required></textarea>
          {{-- <input class="form-control" type="text" id="lokasi_edit" name="lokasi" required> --}}
          <div class="badge badge-warning" id="lokasi_edit_error" hidden></div>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">

        </td>
      </tr>
    </table>
    </form>
        </div>
        <div class="modal-footer">
          <button id="edita" class="btn btn-primary edita" value="tambah">Ubah</button>
          <button type="button" class="btn btn-default btn-warning" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
    </div>
  </div>