<!DOCTYPE html>
<html lang="en">

<head>
@include('flexor.head')
{{-- @extends('layouts.app') --}}
</head>

<body class="page-index has-hero">
  <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
  <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.1">

    {{-- ======== @Region: #navigation ======== --}}
    <div id="navigation" class="wrapper">
      <!--Hidden Header Region-->
    @include('flexor.hheader')
      <!--Header & navbar-branding region-->
    @include('flexor.header')
      @include('flexor.container')
    </div>
    @include('flexor.hero')
  </div>

  {{-- ======== @Region: #content ======== --}}
  @include('flexor.content')

  @include('flexor.clients')
  {{-- ======== @Region: #footer ======== --}}
  @include('flexor.footer')

  <!-- Required JavaScript Libraries -->
  

</body>
<script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/stellar/stellar.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="contactform/contactform.js"></script>
  {{-- <script src="js/app.js"></script> --}}
  <!-- Template Specisifc Custom Javascript File -->
  <script src="js/custom.js"></script>

  <!--Custom scripts demo background & colour switcher - OPTIONAL -->
  <script src="js/color-switcher.js"></script>

  <!--Contactform script -->
  <script src="contactform/contactform.js"></script>
</html>
