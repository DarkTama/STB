<!DOCTYPE html>
<html>
<head>
  @include('includes.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('includes.header')
  @include('projek.sidebar')
  @include('projek.content') 
  @include('includes.modal')  
  {{-- @include('includes.button') --}}
  @include('includes.footer')
  @include('includes.sidebarright')
  <div class="control-sidebar-bg"></div>
</div>
</body>
  @extends('includes.script')
  {{-- @include('pekael.includes.script') --}}
</html>