		$('#adda').click(function(){
			$('#myModal').modal('show');
		})
		$('#nama_tambah').focusout(function(){
			var nama = $('#nama_tambah').val();
			var regex = new RegExp("^[a-zA-Z0-9]+$");
			if (nama== 0){
				$('#nama_error').html('Nama harus diisi').show();
			}
			else if(nama==regex){
				$('#nama_error').html('Hanya Alphabet').show();
			}
			else{
				$('#nama_error').hide();	
			}
			
		})
		$('#kelas_tambah').focusout(function(){
			var nama = $('#kelas_tambah').val();
			if (nama== 0){
				$('#kelas_error').html('Kontak harus diisi').show();
			}
			else{
				$('#kelas_error').hide();	
			}
			
		})
		$('#lokasi_tambah').focusout(function(){
			var nama = $('#lokasi_tambah').val();
			if (nama== 0){
				$('#lokasi_error').html('Alamat harus diisi').show();
			}
			else{
				$('#lokasi_error').hide();	
			}
			
		})
		$('#nama_edit').focusout(function(){
			var nama = $('#nama_edit').val();
			var regex = new RegExp("^[a-zA-Z0-9]+$");
			if (nama== 0){
				$('#nama_edit_error').html('Nama harus diisi').show();
			}
			else if (nama== regex){
				$('#nama_edit_error').html('Hanya Alphabet').show();
			}
			else{
				$('#nama_edit_error').hide();	
			}
			
		})
		$('#kelas_edit').focusout(function(){
			var nama = $('#kelas_edit').val();
			if (nama== 0){
				$('#kelas_edit_error').html('Kontak harus diisi').show();
			}
			else{
				$('#kelas_edit_error').hide();	
			}
			
		})
		$('#lokasi_edit').focusout(function(){
			var nama = $('#lokasi_edit').val();
			if (nama== 0){
				$('#lokasi_edit_error').html('Alamat harus diisi').show();
			}
			else{
				$('#lokasi_edit_error').hide();	
			}
			
		})
		$('#tambah').click(function(e){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
				}
			})
			var type="POST";
			var url="/pkl/tambahan";
			$id = 1;
			var formData = {
				// id: $('#id').val(),
				nama: $('#nama_tambah').val(),
				kelas: $('#kelas_tambah').val(),
				lokasi: $('#lokasi_tambah').val(),
			}
			e.preventDefault();
			console.log(formData);
			$.ajax({
				type: type,
				url: url,
				data: formData,
				dataType: 'json',
				success: function(data){
					var tabel = $('#tabel').DataTable();
					// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
					tabel.ajax.reload();
					console.log(data);
					toastr.success("Berhasil Menambahkan "+data.nama+"!", 'SUKSES!', {timeOut: 2000});
					// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
					$('#formulir').trigger('reset');
					$('#myModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
					console.log(data);
				}
			});
		});
		$(document).on('click', '.edit', function(){
			$('#id_edit').val($(this).data('id'));
			$('#nama_edit').val($(this).data('nama'));
			$('#kelas_edit').val($(this).data('kelas'));
			$('#lokasi_edit').val($(this).data('lokasi'));
			id = $('#id_edit').val();
			$('#editModal').modal('show');
		});
		var id = $('#id_edit').val();
		$('.modal-footer').on('click', '.edit', function(a){
			a.preventDefault();
			$.ajax({
				type: 'PATCH',
				url: '/pkl/updet' + '/' + id,
				dataType: 'json',
				data:{
					_token: $('input[name=_token]').val(),
					id: $('#id_edit').val(),
					nama: $("#nama_edit").val(),
					kelas: $("#kelas_edit").val(),
					lokasi: $("#lokasi_edit").val(),
				},
				success: function(data){
					var tabel = $('#tabel').DataTable();
					tabel.ajax.reload();
					toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
					// $('.siswa'+data.id).replaceWith("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
					$('#editModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Mengubah Data "+data.nama+"!", "GAGAL!", {timeOut: 2000});
					console.log(data);
				}
			})
		})
		$(document).on('click', '.delete', function(){
			$('#id_delete').val($(this).data('id'));
			$('#nama_delete').val($(this).data('nama'));
			$('#kelas_delete').val($(this).data('kelas'));
			$('#lokasi_delete').val($(this).data('lokasi'));
			$('#deleteModal').modal('show');
			id = $('#id_delete').val();
		});
		var id = $('#id_delete').val();
		var siswa = $('.siswa' + id);
		$('.modal-footer').on('click', '.delete', function(a){
			a.preventDefault();
			$.ajax({
				type: 'DELETE',
				dataType: 'json',
				url: 'pkl/pusha' + '/' + id,
				data: {
					nama : $("#nama_delete").val(),
					'_token': $('input[name=_token]').val(),
				},
				success:function(data){
					var tabel = $('#tabel').DataTable();
					tabel.ajax.reload();
					toastr.success("Berhasil Menghapus Data!", 'SUKSES!', {timeOut: 2000});
					// $('.siswa' + id).remove();
					$('#deleteModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Menghapus Data "+data.nama+"!", 'GAGAL!', {timeOut: 2000});
					console.log(data);
				}
			});
			return false;
		});
	$(document).ready(function(){
			var a = $('#tabel').DataTable({
				"ajax":"/pkl/bawa",
				"columns":[
				{"data":"id"},
				{"data":"nama"},
				{"data":"kelas"},
				{"data":"lokasi"},
				
				
				],
				// "columnDefs": [ {
	   //          "searchable": false,
	   //          "orderable": false,
	   //          "targets": 0
		  //       } ],
		  //       "order": [[ 1, 'asc' ]],
		        {bSortable: false,
					data:null,
					className: "center",
					render: function(a){
						return"<button class='btn btn-warning btn-sm edit' data-id='"+ a.id +"' data-nama="+a.nama+" data-kelas="+a.kelas+" data-lokasi="+a.lokasi+">Edit</button> <button class='btn btn-danger btn-sm delete' data-id="+a.id+" data-nama="+a.nama+" data-kelas="+a.kelas+" data-lokasi="+a.lokasi+">Hapus</button>";
					},
				},
			});
			// a.on( 'order.dt search.dt', function () {
   //      	a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
   //          cell.innerHTML = i+1;
   //      	});
   // 		 	}).draw();
		});
	$('#tambaha').click(function(e){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
				}
			})
			// var id = $('');
			var type="POST";
			var url="/tambahpost";
			$id = 1;
			var formData = {
				// id: $('#id').val(),
				user_id: $('#nama_tambah').val(),
				// user_id: id,
				title: $('#kelas_tambah').val(),
				informasi: $('#lokasi_tambah').val(),
			}
			e.preventDefault();
			console.log(formData);
			$.ajax({
				type: type,
				url: url,
				data: formData,
				dataType: 'json',
				success: function(data){
					var tabel = $('#tabled').DataTable();
					// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
					tabel.ajax.reload();
					console.log(data);
					toastr.success("Berhasil Menambahkan "+data.title+"!", 'SUKSES!', {timeOut: 2000});
					// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
					$('#formulir').trigger('reset');
					$('#myModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
					console.log(data);
				}
			});
		});
	$(document).on('click', '.edita', function(){
			$('#id_edit').val($(this).data('id'));
			$('#nama_edit').val($(this).data('nama'));
			$('#kelas_edit').val($(this).data('kelas'));
			$('#lokasi_edit').val($(this).data('lokasi'));
			id = $('#id_edit').val();
			$('#editModal').modal('show');
		});
		var id = $('#id_edit').val();
		$('.modal-footer').on('click', '.edita', function(a){
			a.preventDefault();
			$.ajax({
				type: 'PATCH',
				url: '/update' + '/' + id,
				dataType: 'json',
				data:{
					_token: $('input[name=_token]').val(),
					// id: $('#id_edit').val(),
					// nama: $("#nama_edit").val(),
					title: $("#kelas_edit").val(),
					informasi: $("#lokasi_edit").val(),
				},
				success: function(data){
					var tabel = $('#tabled').DataTable();
					tabel.ajax.reload();
					toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
					// $('.siswa'+data.id).replaceWith("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
					$('#editModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Mengubah Data "+data.nama+"!", "GAGAL!", {timeOut: 2000});
					console.log(data);
				}
			})
		})
		$(document).on('click', '.deletea', function(){
			$('#id_delete').val($(this).data('id'));
			$('#nama_delete').val($(this).data('nama'));
			$('#kelas_delete').val($(this).data('kelas'));
			$('#lokasi_delete').val($(this).data('lokasi'));
			$('#deleteModal').modal('show');
			id = $('#id_delete').val();
		});
		var id = $('#id_delete').val();
		var siswa = $('.siswa' + id);
		$('.modal-footer').on('click', '.deletea', function(a){
			a.preventDefault();
			$.ajax({
				type: 'DELETE',
				dataType: 'json',
				url: 'del' + '/' + id,
				data: {
					// id: $
					id : $("#id_delete").val(),
					'_token': $('input[name=_token]').val(),
				},
				success:function(data){
					var tabel = $('#tabled').DataTable();
					tabel.ajax.reload();
					toastr.success("Berhasil Menghapus Data!", 'SUKSES!', {timeOut: 2000});
					// $('.siswa' + id).remove();
					$('#deleteModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Menghapus Data "+data.nama+"!", 'GAGAL!', {timeOut: 2000});
					console.log(data);
				}
			});
			return false;
		});
		$(document).on('click', '#edi', function(){
			$('#id_edit').val($(this).data('id'));
			$('#nama_edit').val($(this).data('nama'));
			$('#kelas_edit').val($(this).data('kelas'));
			$('#lokasi_edit').val($(this).data('lokasi'));
			id = $('#id_edit').val();
			$('#editModal').modal('show');
		});
		var id = $('#id_edit').val();
		$('.modal-footer').on('click', '.edi', function(a){
			a.preventDefault();
			$.ajax({
				type: 'PATCH',
				url: '/update' + '/' + id,
				dataType: 'json',
				data:{
					_token: $('input[name=_token]').val(),
					// id: $('#id_edit').val(),
					// nama: $("#nama_edit").val(),
					title: $("#kelas_edit").val(),
					informasi: $("#lokasi_edit").val(),
				},
				success: function(data){
					var tabel = $('#tabled').DataTable();
					tabel.ajax.reload();
					toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
					// $('.siswa'+data.id).replaceWith("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
					$('#editModal').modal('hide');
				},
				error:function(data){
					toastr.error("Gagal Mengubah Data "+data.nama+"!", "GAGAL!", {timeOut: 2000});
					console.log(data);
				}
			})
		})
		$(document).ready(function(){
			$('#tabeld').DataTable({
				"ajax":"/pkl/bawa",
				"columns":[
				{"data":"id"},
				{"data":"nama"},
				{"data":"kelas"},
				{"data":"lokasi"},
				],
			});
		});