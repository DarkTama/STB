
$(document).on('click', '.editProf', function(){
	$('#idprof').val($('#idi').data('id'));
	$('#slogan_edit').val($('#idi').data('slogan'));
	$('#highlight_edit').val($('#idi').data('highlight'));
	$('#profil_edit').val($('#idi').data('profil'));
	$('#editProf').modal('show');
	// var id = $('#idi').val($(this).data('id'));
});
// $('.modal-footer').on('click', '.edit', function(a){
$('#edit').click(function(a){
	var id = $('#idprof').val();
	a.preventDefault();
	$.ajax({
		type: 'patch',
		url: 'editprof'+ '/' +id,
		dataType: 'json',
		data:{
			_token: $('input[name=_token]').val(),
			id: id,
			slogan: $("#slogan_edit").val(),
			highlight: $("#highlight_edit").val(),
			profil: $("#profil_edit").val(),
		},
		success: function(data){
			// var tabel = $('#tabel').DataTable();
			// tabel.ajax.reload();
			toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
			// $('.siswa'+data.id).replaceWith("");
			console.log(data);
			$('#slogan').replaceWith(data.slogan);
			$('#highlight').replaceWith(data.highlight);
			$('#profil').replaceWith(data.profil);
			$('#editProf').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengubah Data!", "GAGAL!", {timeOut: 2000});
			console.log(data);
		}
	});
});

$(document).on('click', '.tambahKontak', function(){
	// $('#slogan_edit').val($('#slogan').text());
	// $('#highlight_edit').val($('#highlight').text());
	// $('#profil_edit').val($('#profil').text());
	$('#formulir').trigger('reset');
	$('#tambah').modal('show');
	// var id = $('#idi').val($(this).data('id'));
});
$(document).on('click', '.EditKontak', function(){
	$('#ide').val($(this).data('id'));
	$('#jenis_edit').val($(this).data('jenis'));
	$('#kontak_edit').val($(this).data('kontak'));
	$('#ikon_edit').val($(this).data('ikon'));
	$('#urut_edit').val($(this).data('urut'));
	$('#edi').modal('show');
})
$('#ediKontak').click(function(e){
	var id = $('#ide').val();
	e.preventDefault();
	$.ajax({
		type: 'PATCH',
		url: 'editKontak' + '/' + id,
		dataType: 'json',
		data:{
			_token: $('input[name=_token]').val(),
			// id: $('#ide').val(),
			jenis: $("#jenis_edit").val(),
			kontak: $("#kontak_edit").val(),
			ikon: $("#ikon_edit").val(),
			urut: $("#urut_edit").val(),
		},
		success: function(data){
			var tabel = $('#tabelKontak').DataTable();
			tabel.ajax.reload();
			toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
			// $('.siswa'+data.id).replaceWith("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#edi').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengubah Data "+data.nama+"!", "GAGAL!", {timeOut: 2000});
			console.log(data);
		}
	})
})
$(document).on('click', '.HapusKontaks', function(){
	$('#idids').val($(this).data('id'));
	$('#hapus').modal('show');
})
$('#del').click(function(e){
	var id = $('#idids').val();
	e.preventDefault();
	$.ajax({
		type: 'DELETE',
		url: 'hapusKontak'+ '/' + id,
		dataType: 'json',
		data:{
			_token:$('input[name=_token]').val(),
			id: id,
		},
		success:function(data){
			console.log(data);
			var tabel = $('#tabelKontak').DataTable();
			tabel.ajax.reload();
			toastr.success("Berhasil Menghapus Data!", "SUKSES!", {timeOut:2000});
			$('#hapus').modal('hide');
		},
		error:function(data){
			console.log(data);
			toastr.error("Gagal Menghapus Data!", "GAGAL!", {timeOut:2000});
		}
	})
})
$('#addKontak').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	// var id = $('');
	var type="POST";
	var url="tambahKontak";
	$id = 1;
	var formData = {
		// id: $('#id').val(),
		jenis: $('#jenis').val(),
		// user_id: id,
		kontak: $('#kontak').val(),
		ikon: $('#ikon').val(),
		urut: $('#urut').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelKontak').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambah').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.tambahSosmed', function(){
	$('#formulir').trigger('reset');
	$('#tambahSosmed').modal('show');
})
$('#addSosmed').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content'),
		}
	})
	// var id = $('');
	var type="POST";
	var url="tambahSosmed";
	// $id = 1;
	var formData = {
		// id: $('#id').val(),
		jeniss: $('#jeniss').val(),
		// user_id: id,
		sosmed: $('#sosmed').val(),
		ikons: $('#ikons').val(),
		uruts: $('#uruts').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelSosmed').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambahSosmed').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditSosmed', function(){
	$('#idsosmed').val($(this).data('id'));
	$('#jenisedit').val($(this).data('jenis'));
	$('#sosmededits').val($(this).data('sosmed'));
	$('#ikonedit').val($(this).data('ikon'));
	$('#urutedit').val($(this).data('urut'));
	$('#ediSosmed').modal('show');
})
$('#EditSosmed').click(function(e){
	var id = $('#idsosmed').val();
	e.preventDefault();
	$.ajax({
		type: 'PATCH',
		url: 'editSosmed' + '/' + id,
		dataType: 'json',
		data:{
			_token: $('input[name=_token]').val(),
			// id: $('#ide').val(),
			jenis: $("#jenisedit").val(),
			sosmed: $("#sosmededits").val(),
			ikon: $("#ikonedit").val(),
			urut: $("#urutedit").val(),
		},
		success: function(data){
			var tabel = $('#tabelSosmed').DataTable();
			tabel.ajax.reload();
			toastr.success("Berhasil Mengubah Data!", 'SUKSES!', {timeOut: 2000});
			// $('.siswa'+data.id).replaceWith("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#ediSosmed').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengubah Data "+data.nama+"!", "GAGAL!", {timeOut: 2000});
			console.log(data);
		}
	})
})
$(document).on('click', '.HapusSosmed', function(){
	$('#idhapus').val($(this).data('id'));
	$('#hapusSosmed').modal('show');
})
$('#dele').click(function(e){
	var id = $('#idhapus').val();
	e.preventDefault();
	$.ajax({
		type: 'DELETE',
		url: 'hapusSosmed'+ '/' + id,
		dataType: 'json',
		data:{
			_token:$('input[name=_token]').val(),
			id: id,
		},
		success:function(data){
			console.log(data);
			var tabel = $('#tabelSosmed').DataTable();
			tabel.ajax.reload();
			toastr.success("Berhasil Menghapus Data!", "SUKSES!", {timeOut:2000});
			$('#hapusSosmed').modal('hide');
		},
		error:function(data){
			console.log(data);
			toastr.error("Gagal Menghapus Data!", "GAGAL!", {timeOut:2000});
		}
	})
})
$('.profil').readmore();
$(document).on('click', '.tambahOffice', function(){
	$('#formulir').trigger('reset');
	$('#tambahOffice').modal('show');
})
$('#addOffices').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	// var id = $('');
	var type="POST";
	var url="tambahOffice";
	// $id = 1;
	var formData = {
		// id: $('#id').val(),
		nama: $('#namaoffice').val(),
		alamat: $('#alamatoffice').val(),
		kota: $('#kotaoffice').val(),
		kodepos: $('#kodepos').val(),
		telephone: $('#telephone').val(),
		koordinat: $('#koordinat').val(),
		aktif: $('#aktifoffice:checked').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelOffice').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambahOffice').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditOffice', function(){
	$('#idofficeedit').val($(this).data('id'));
	$('#namaofficeedit').val($(this).data('nama'));
	$('#alamatofficeedit').val($(this).data('alamat'));
	$('#kotaofficeedit').val($(this).data('kota'));
	$('#kodeposedit').val($(this).data('kodepos'));
	$('#telephoneedit').val($(this).data('telephone'));
	$('#koordinatedit').val($(this).data('koordinat'));
	$('#aktifofficeedit:checked').val($(this).data('aktif'));
	$('#ediOffice').modal('show');
})
$('#editOffices').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idofficeedit').val();
	var type="PATCH";
	var url="editOffice" + "/" + id;
	// $id = 1;
	var formData = {
		id: id,
		nama: $('#namaofficeedit').val(),
		alamat: $('#alamatofficeedit').val(),
		kota: $('#kotaofficeedit').val(),
		kodepos: $('#kodeposedit').val(),
		telephone: $('#telephoneedit').val(),
		koordinat: $('#koordinatedit').val(),
		aktif: $('#aktifofficeedit:checked').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelOffice').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#ediOffice').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusOffice', function(){
	$('#idhapusoffice').val($(this).data('id'));
	$('#hapusOffice').modal('show');
})
$('#delet').click(function(e){
	var id = $('#idhapusoffice').val();
	e.preventDefault();
	$.ajax({
		type: 'DELETE',
		url: 'hapusOffice'+ '/' + id,
		dataType: 'json',
		data:{
			_token:$('input[name=_token]').val(),
			id: id,
		},
		success:function(data){
			console.log(data);
			var tabel = $('#tabelOffice').DataTable();
			tabel.ajax.reload();
			toastr.success("Berhasil Menghapus Data!", "SUKSES!", {timeOut:2000});
			$('#hapusOffice').modal('hide');
		},
		error:function(data){
			console.log(data);
			toastr.error("Gagal Menghapus Data!", "GAGAL!", {timeOut:2000});
		}
	})
})

$('#addJenis').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	// var id = $('#idjpsedit').val();
	var type="POST";
	var url="tambahJenis";
	// $id = 1;
	var formData = {
		// id: id,
		jenis: $('#jenisjps').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelJenis').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambahJenis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
})
$(document).on('click', '.EditJenis', function(){
	$('#idjpsedit').val($(this).data('id'));
	$('#jenisjpsedit').val($(this).data('jps'));
	$('#ediJenis').modal('show');
})
$('#editJenise').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idjpsedit').val();
	var type="PATCH";
	var url="editJenis"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		jenis: $('#jenisjpsedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelJenis').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#ediJenis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
})
$(document).on('click', '.HapusJenis', function(){
	$('#idjpshapus').val($(this).data('id'));
	$('#hapusJenis').modal('show');
})
$('#deleteJenis').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idjpshapus').val();
	var type="DELETE";
	var url="hapusJenis"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		// jenis: $('#jenisjpsedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelJenis').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#hapusJenis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
})
$('#addVideo').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	// var id = $('');
	var type="POST";
	var url="tambahVideo";
	// $id = 1;
	var formData = {
		// id: $('#id').val(),
		judul: $('#judulvid').val(),
		link: $('#linkvid').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelVideo').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambahVideo').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditVideo', function(){
	$('#idvidedit').val($(this).data('id'));
	$('#judulvidedit').val($(this).data('judul'));
	$('#linkvidedit').val($(this).data('link'));
	$('#ediVideo').modal('show');
})
$('#editVideo').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idvidedit').val();
	var type="PATCH";
	var url="editVideo"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		judul: $('#judulvidedit').val(),
		link: $('#linkvidedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelVideo').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#ediVideo').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
})
$(document).on('click', '.HapusVideo', function(){
	$('#idvidhapus').val($(this).data('id'));
	$('#hapusVideo').modal('show');
})
$('#deleted').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idvidhapus').val();
	var type="DELETE";
	var url="hapusVideo"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		// judul: $('#judulvidedit').val(),
		// link: $('#linkvidedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelVideo').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#hapusVideo').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addSlider').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var posisisld = $("input:radio[name=posisisld]:checked").val();
	var [posisigambar, posisiteks] = posisisld.split(":");
	var type="POST";
	var url="tambahSlider";
	// $id = 1;
	var formData = {
		// id: $('#id').val(),
		judul: $('#judulsld').val(),
		subjudul: $('#subjudulsld').val(),
		prakata: $('#prakatasld').val(),
		deskripsi: $('#deskripsisld').val(),
		posisigambar: posisigambar,
		posisiteks: posisiteks,
		urut: $('#urutsld').val(),
		aktif: $('#aktifsld:checked').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelSlider').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#tambahSlider').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditSlider', function(){
	$('#idsldedit').val($(this).data('id'));
	$('#judulsldedit').val($(this).data('judul'));
	$('#subjudulsldedit').val($(this).data('subjudul'));
	$('#prakatasldedit').val($(this).data('prakata'));
	$('#deskripsisldedit').val($(this).data('deskripsi'))
	$('#urutsldedit').val($(this).data('urut'));
	$('#ediSlider').modal('show');
})
$('#editSlider').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var posisisld = $("input:radio[name=posisisldedit]:checked").val();
	var [posisigambar, posisiteks] = posisisld.split(":");
	var type= "PATCH";
	var id= $('#idsldedit').val();
	var url= "editSlider"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		judul: $('#judulsldedit').val(),
		subjudul: $('#subjudulsldedit').val(),
		prakata: $('#prakatasldedit').val(),
		deskripsi: $('#deskripsisldedit').val(),
		posisigambar: posisigambar,
		posisiteks: posisiteks,
		urut: $('#urutsldedit').val(),
		aktif: $('#aktifsldedit:checked').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelSlider').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#ediSlider').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusSlider', function(){
	$('#idhapussld').val($(this).data('id'));
	$('#hapusSlider').modal('show');
});
$('#deletesld').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id = $('#idhapussld').val();
	var type="DELETE";
	var url="hapusSlider"+"/"+id;
	// $id = 1;
	var formData = {
		id: id,
		// judul: $('#judulvidedit').val(),
		// link: $('#linkvidedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelSlider').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menghapus!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#hapusSlider').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menghapus Data!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.pictureShow', function(){
	$('.modal-title').replaceWith($(this).data('nama'));
	$('#picture').attr('src', $(this).data('lokasi'));
	$('#pictureShow').modal('show');
});
$('#uploadImageSlider').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	// var id = $('#idhapussld').val();
	var type="POST";
	var url="uploadpicturesld";
	// $id = 1;;
	e.preventDefault();
	var formData;
	// console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: new formData($('#UploadPic')[0]),
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelSlider').DataTable();
			// var rn = tabel.row.add([ data.id, data.nama, data.kelas, data.lokasi ]).draw();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Mengunggah Foto!", 'SUKSES!', {timeOut: 2000});
			// $('#datan').append("<tr class='siswa"+data.id+"'><td id='id'>" + data.id +"</td><td id='nama'>" +data.nama+ "</td><td id='kelas'>" +data.kelas+ "</td><td id='lokasi'>" +data.lokasi+ "</td></tr>");
			$('#formulir').trigger('reset');
			$('#hapusSlider').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengunggah Foto!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.pictureUpload', function(){
	$('#picturesldid').val($(this).data('id'));
	$('#uploadpicSlider').modal('show');
});
$('#addProduk').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahProduk";
	var formData = {
		jps: $('#jenisProduk').val(),
		produk: $('#namaproduk').val(),
		deskripsi: $('#deskripsiproduk').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProduk').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahProduks').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditProduk', function(){
	$('#idProduk').val($(this).data('id'));
	$('#jenisProdukedit').val($(this).data('jps'));
	$('#namaprodukedit').val($(this).data('produk'));
	$('#deskripsiprodukedit').val($(this).data('deskripsi'));
	$('#ediProduk').modal('show');
});
$('#editProduk').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idProduk').val();
	var type="PATCH";
	var url="editProduk"+"/"+id;
	var formData = {
		id: id,
		jps: $('#jenisProdukedit').val(),
		produk: $('#namaprodukedit').val(),
		deskripsi: $('#deskripsiprodukedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProduk').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediProduk').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusProduk', function(){
	$('#idhapusproduk').val($(this).data('id'));
	$('#hapusProduk').modal('show');
});
$('#deleteproduk').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusproduk').val();
	var type="DELETE";
	var url="hapusProduk"+"/"+id;
	var formData = {
		id: id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProduk').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusProduk').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.pictureUploadProduk', function(){
	$('#pictureprodukid').val($(this).data('id'));
	$('#uploadpicProduk').modal('show');
});
$(document).on('click', '.pictureUploadProduks', function(){
	$('#pictureprodukids').val($(this).data('id'));
	$('#uploadpicProduks').modal('show');
});
$(document).on('click', '.pictureShows', function(){
	$('#pictureShowsID').val($(this).data('id'));
	$('#pictureShows').modal('show');
});

$(document).on('click', '.HapusProdukGambar', function(){
	$('#idhapusprodukgambar').val($(this).data('id'));
	$('#hapusProdukGambar').modal('show');
});
$('#deleteprodukgambar').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusprodukgambar').val();
	var type="DELETE";
	var url="hapusProdukGambar"+"/"+id;
	var formData = {
		id: id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProduk').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusProdukGambar').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addProjek').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahProjek";
	var formData = {
		jps: $('#jenisProjek').val(),
		projek: $('#namaprojek').val(),
		deskripsi: $('#deskripsiprojek').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProjek').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahProjek').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditProjek', function(){
	$('#idProjek').val($(this).data('id'));
	$('#jenisProjekedit').val($(this).data('jps'));
	$('#namaprojekedit').val($(this).data('projek'));
	$('#deskripsiprojekedit').val($(this).data('deskripsi'));
	$('#ediProjek').modal('show');
});
$('#editProjek').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idProjek').val();
	var type="PATCH";
	var url="editProjek"+"/"+id;
	var formData = {
		id: id,
		jps: $('#jenisProjekedit').val(),
		projek: $('#namaprojekedit').val(),
		deskripsi: $('#deskripsiprojekedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProjek').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediProjek').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusProjek', function(){
	$('#idhapusprojek').val($(this).data('id'));
	$('#hapusProjek').modal('show');
});
$('#deleteprojek').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusprojek').val();
	var type="DELETE";
	var url="hapusProjek"+"/"+id;
	var formData = {
		id: id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelProjek').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menghapus!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusProjek').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menghapus!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.pictureUploadProjek', function(){
	$('#pictureprojekid').val($(this).data('id'));
	$('#uploadpicProjek').modal('show');
});
$('#addServis').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahServis";
	var formData = {
		jps: $('#jenisServis').val(),
		servis: $('#namaservis').val(),
		deskripsi: $('#deskripsiservis').val(),
		ikon: $('#ikonservis').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelServis').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahServis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditServis', function(){
	$('#idServis').val($(this).data('id'));
	$('#jenisServisedit').val($(this).data('jps'));
	$('#namaservisedit').val($(this).data('servis'));
	$('#deskripsiservisedit').val($(this).data('deskripsi'));
	$('#ikonservisedit').val($(this).data('ikon'));
	$('#ediServis').modal('show');
})
$('#editServis').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idServis').val();
	var type="PATCH";
	var url="editServis"+"/"+id;
	var formData = {
		id: id,
		jps: $('#jenisServisedit').val(),
		servis: $('#namaservisedit').val(),
		deskripsi: $('#deskripsiservisedit').val(),
		ikon: $('#ikonservisedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelServis').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediServis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusServis', function(){
	$('#idhapusservis').val($(this).data('id'));
	$('#hapusServis').modal('show');
});
$('#deleteservis').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusservis').val();
	var type="DELETE";
	var url="hapusServis"+"/"+id;
	var formData = {
		id: id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelServis').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusServis').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addBlog').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahBlog";
	var formData = {
		penulis: $('#penulisblog').val(),
		judul: $('#judulblog').val(),
		isi: $('#isiblog').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelBlog').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahBlog').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditBlog', function(){
	$('#idblog').val($(this).data('id'));
	$('#penulisblogedit').val($(this).data('penulis'));
	$('#judulblogedit').val($(this).data('judul'));
	$('#isiblogedit').val($(this).data('isi'));
	$('#ediBlog').modal('show');
});
$('#editBlog').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="PATCH";
	var id=$('#idblog').val();
	var url="editBlog"+"/"+id;
	var formData = {
		penulis: $('#penulisblogedit').val(),
		judul: $('#judulblogedit').val(),
		isi: $('#isiblogedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelBlog').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediBlog').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusBlog', function(){
	$('#idhapusblog').val($(this).data('id'));
	$('#hapusBlog').modal('show');
});
$('#deleteblog').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="DELETE";
	var id=$('#idhapusblog').val();
	var url="hapusBlog"+"/"+id;
	var formData = {
		id: id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelBlog').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusBlog').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addKlien').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahKlien";
	var formData = {
		klien: $('#klien').val(),
		alamat: $('#alamatklien').val(),
		telp: $('#teleponklien').val(),
		email: $('#emailklien').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelKlien').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahKlien').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditKlien', function(){
	$('#idklien').val($(this).data('id'));
	$('#klienedit').val($(this).data('klien'));
	$('#alamatklienedit').val($(this).data('alamat'));
	$('#teleponklienedit').val($(this).data('telepon'));
	$('#emailklienedit').val($(this).data('email'));
	$('#ediKlien').modal('show');
});
$('#editKlien').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idklien').val();
	var type="PATCH";
	var url="editKlien"+"/"+id;
	var formData = {
		id:id,
		klien: $('#klienedit').val(),
		alamat: $('#alamatklienedit').val(),
		telp: $('#teleponklienedit').val(),
		email: $('#emailklienedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelKlien').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Mengubah!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediKlien').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengubah!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusKlien', function(){
	$('#idhapusklien').val($(this).data('id'));
	$('#hapusKlien').modal('show');
});
$('#deleteklien').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusklien').val();
	var type="DELETE";
	var url="hapusKlien"+"/"+id;
	var formData = {
		id:id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelKlien').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menghapus!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusKlien').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menghapus!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addTesti').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahTesti";
	var formData = {
		klien: $('#klientesti').val(),
		nama: $('#namatesti').val(),
		testi: $('#testimoni').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelTesti').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahTesti').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditTesti', function(){
	$('#idtesti').val($(this).data('id'));
	$('#klientestiedit').val($(this).data('klien'));
	$('#namatestiedit').val($(this).data('nama'));
	$('#testimoniedit').val($(this).data('testimoni'));
	$('#ediTesti').modal('show');
});
$('#editTesti').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idtesti').val();
	var type="PATCH";
	var url="editTesti"+"/"+id;
	var formData = {
		id:id,
		klien: $('#klientestiedit').val(),
		nama: $('#namatestiedit').val(),
		testi: $('#testimoniedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelTesti').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediTesti').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusTesti', function(){
	$('#idhapustesti').val($(this).data('id'));
	$('#hapusTesti').modal('show');
});
$('#deletetesti').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapustesti').val();
	var type="DELETE";
	var url="hapusTesti"+"/"+id;
	var formData = {
		id:id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelTesti').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusTesti').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addPartner').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahPartner";
	var formData = {
		partner: $('#partner').val(),
		alamat: $('#alamatpartner').val(),
		telp: $('#teleponpartner').val(),
		email: $('#emailpartner').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelPartner').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahPartner').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditPartner', function(){
	$('#idpartner').val($(this).data('id'));
	$('#partneredit').val($(this).data('partner'));
	$('#alamatpartneredit').val($(this).data('alamat'));
	$('#teleponpartneredit').val($(this).data('telepon'));
	$('#emailpartneredit').val($(this).data('email'));
	$('#ediPartner').modal('show');
});
$('#editPartner').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idpartner').val();
	var type="PATCH";
	var url="editPartner"+"/"+id;
	var formData = {
		id:id,
		partner:$('#partneredit').val(),
		alamat:$('#alamatpartneredit').val(),
		telp:$('#teleponpartneredit').val(),
		email:$('#emailpartneredit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelPartner').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Mengubah!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediPartner').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Mengubah!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusPartner', function(){
	$('#idhapuspartner').val($(this).data('id'));
	$('#hapusPartner').modal('show');
});
$('#deletepartner').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapuspartner').val();
	var type="DELETE";
	var url="hapusPartner"+"/"+id;
	var formData = {
		id:id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelPartner').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menghapus!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusPartner').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menghapus!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.pictureUploadBlog', function(){
	$('#pictureblogid').val($(this).data('id'));
	$('#uploadpicBlog').modal('show');
});
$(document).on('click', '.pictureUploadKlien', function(){
	$('#pictureklienid').val($(this).data('id'));
	$('#uploadpicKlien').modal('show');
});
$(document).on('click', '.pictureUploadTesti', function(){
	$('#picturetestiid').val($(this).data('id'));
	$('#uploadpicTesti').modal('show');
});
$(document).on('click', '.pictureUploadPartner', function(){
	$('#picturepartnerid').val($(this).data('id'));
	$('#uploadpicPartner').modal('show');
});
$(document).on('click', '.pictureUploadUsers', function(){
	$('#pictureusersid').val($(this).data('id'));
	$('#uploadpicUsers').modal('show');
});
$('#addUnduhan').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahUnduhan";
	var formData = {
		judul: $('#judulunduhan').val(),
		link: $('#linkunduhan').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUnduhan').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahUnduhan').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditUnduhan', function(){
	$('#idunduhan').val($(this).data('id'));
	$('#judulunduhanedit').val($(this).data('judul'));
	$('#linkunduhanedit').val($(this).data('link'));
	$('#ediUnduhan').modal('show');
});
$('#editUnduhan').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idunduhan').val();
	var type="PATCH";
	var url="editUnduhan"+"/"+id;
	var formData = {
		id:id,
		judul: $('#judulunduhanedit').val(),
		link: $('#linkunduhanedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUnduhan').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediUnduhan').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusUnduhan', function(){
	$('#idhapusunduhan').val($(this).data('id'));
	$('#hapusUnduhan').modal('show');
});
$('#deleteunduhan').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapusunduhan').val();
	var type="DELETE";
	var url="hapusUnduhan"+"/"+id;
	var formData = {
		id:id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUnduhan').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusUnduhan').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$('#addUsers').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var type="POST";
	var url="tambahUsers";
	var formData = {
		email: $('#emailusers').val(),
		nama: $('#namausers').val(),
		status: $('#statususers:checked').val(),
		jenis: $('#jenisusers').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUsers').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#tambahUsers').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.EditUsers', function(){
	$('#idusers').val($(this).data('id'));
	$('#emailusersedit').val($(this).data('email'));
	$('#namausersedit').val($(this).data('nama'));
	$('#statususersedit').val($(this).data('status'));
	$('#jenisusersedit').val($(this).data('jenis'));
	$('#ediUsers').modal('show');
});
$('#editUsers').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idusers').val();
	var type="PATCH";
	var url="editUsers"+"/"+id;
	var formData = {
		id:id,
		email: $('#emailusersedit').val(),
		nama: $('#namausersedit').val(),
		status: $('#statususersedit:checked').val(),
		jenis: $('#jenisusersedit').val(),
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUsers').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#ediUsers').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).on('click', '.HapusUsers', function(){
	$('#idhapususers').val($(this).data('id'));
	$('#hapusUsers').modal('show');
});
$('#deleteusers').click(function(e){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
		}
	})
	var id=$('#idhapususers').val();
	var type="DELETE";
	var url="hapusUsers"+"/"+id;
	var formData = {
		id:id,
	}
	e.preventDefault();
	console.log(formData);
	$.ajax({
		type: type,
		url: url,
		data: formData,
		dataType: 'json',
		success: function(data){
			var tabel = $('#tabelUsers').DataTable();
			tabel.ajax.reload();
			console.log(data);
			toastr.success("Berhasil Menambahkan!", 'SUKSES!', {timeOut: 2000});
			$('#formulir').trigger('reset');
			$('#hapusUsers').modal('hide');
		},
		error:function(data){
			toastr.error("Gagal Menambahkan!", 'GAGAL!', {timeOut: 2000});
			console.log(data);
		}
	});
});
$(document).ready(function(){
	$('#tabelPicture').DataTable({
		"ajax":"admin/picture",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<img src='/liravel/public/"+data.lokasi+"'></img>";
			},

		},
		],
	});
	$(window).scroll(function(){
		if($(this).scrollTop()>0){
			$('header').addClass('ddd');
			$('.sidebar').addClass('dddd');
		}
		else{
			$('header').removeClass('ddd');
			$('.sidebar').removeClass('dddd');
		}
	});
	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		}); 	
	var a = $('#tabelKontak').DataTable({
		"ajax":"admin/kontak",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a data-toggle='modal' data-target='#edi' href='#' class='EditKontak' id='EditKontak' data-id='"+a.id_ktk+"' data-jenis='"+a.jnskontak+"' data-kontak='"+a.dtlkontak+"' data-ikon='"+a.ikon+"' data-urut='"+a.urut+"'><span class='fa fa-edit'></span>Edit</a></li><li><a data-toggle='modal' data-target='#hapus' href='#' class='HapusKontaks' id='HapusKontak' data-id='"+a.id_ktk+"'><span class='fa fa-remove'></span>Hapus</a></li></ul></li></div>";
			},

		},
		{"data":"id_ktk"},
		{"data":"jnskontak"},
		{"data":"dtlkontak"},
		{"data":"ikon"},
		{"data":"urut"},
		],
		"order":[[0, 'asc']],
	});
	a.on('order.dt search.dt', function(){
		a.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var b = $('#tabelSosmed').DataTable({
		"ajax":"admin/sosmed",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a data-toggle='modal' data-target='#ediSosmed' href='#' class='EditSosmed' id='EditSosmed' data-id='"+a.id_sosmed+"' data-jenis='"+a.jnssosmed+"' data-sosmed='"+a.dtlsosmed+"' data-ikon='"+a.ikon+"' data-urut='"+a.urut+"'><span class='fa fa-edit'></span>Edit</a></li><li><a data-toggle='modal' data-target='#hapusSosmed' href='#' class='HapusSosmed' id='HapusSosmed' data-id='"+a.id_sosmed+"'><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_sosmed"},
		{"data":"jnssosmed"},
		{"data":"dtlsosmed"},
		{"data":"ikon"},
		{"data":"urut"},
		],
		"order":[[0, 'asc']],
	});
	b.on('order.dt search.dt', function(){
		b.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var c = $('#tabelOffice').DataTable({
		"ajax":"admin/office",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediOffice' data-id='"+a.id_off+"' data-nama='"+a.nama+"' data-alamat='"+a.alamat+"' data-kota='"+a.kota+"' data-kodepos='"+a.kodepos+"' data-koordinat='"+a.kordinat+"' data-telephone='"+a.telp+"' data-aktif='"+a.aktif+"' class='EditOffice' id='EditOffice'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusOffice' class='HapusOffice' data-id="+a.id_off+" id='HapusOffice'><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_off"},
		{"data":"nama"},
		{"data":"alamat"},
		{"data":"kota"},
		{"data":"kodepos"},	
		{"data":"telp"},
		{"data":"kordinat"},
		// {"data":"aktif"},
		{
                data:   "aktif",
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="editor-active" disabled>';
                    }
                    return data;
                },
                className: "dt-body-center"
            }
		],
		rowCallback: function ( row, data ) {
            // Set the checked state of the checkbox in the table
            $('input.editor-active', row).prop( 'checked', data.aktif == 1 );
        },
        "order":[[0, 'asc']],
	});
	c.on('order.dt search.dt', function(){
		c.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var d = $('#tabelJenis').DataTable({
		"ajax":"jenis",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediJenis' data-id='"+a.id_jps+"' data-jps='"+a.jps+"' class='EditJenis' id='EditJenis'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusJenis' class='HapusJenis' data-id="+a.id_jps+" id='HapusJenis'><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_jps"},
		{"data":"jps"},
		],
		"order":[[0, 'asc']],
	});
	d.on('order.dt search.dt', function(){
		d.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var e = $('#tabelVideo').DataTable({
		"ajax":"video",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediVideo' data-id='"+a.id_vid+"' data-judul='"+a.judulvid+"' data-link='"+a.linkvid+"' class='EditVideo' id='EditVideo'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusVideo' class='HapusVideo' id='HapusVideo' data-id="+a.id_vid+"><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_vid"},
		{"data":"judulvid"},
		{"data":"linkvid"},
		],
		"order":[[0, 'asc']],
	});
	e.on('order.dt search.dt', function(){
		e.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var f = $('#tabelSlider').DataTable({
		"ajax":"slider",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediSlider' data-id='"+a.id_sld+"' data-judul='"+a.judulsld+"' data-subjudul='"+a.subjudul+"' data-prakata='"+a.prakata+"' data-deskripsi='"+a.deskripsi+"' data-posisi='"+a.posisi+"' data-urut='"+a.urut+"' data-aktif='"+a.aktif+"' class='EditSlider' id='EditSlider'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusSlider' class='HapusSlider' data-id="+a.id_sld+" id='HapusSlider'><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicSlider' class='pictureUpload' data-id="+a.id_sld+"><span class='fa fa-upload'></span>Unggah Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_sld"},
		{"data":"judulsld"},
		{"data":"subjudul"},
		{"data":"prakata"},
		// {"data":"deskripsi"},
		// {"data":"posisi"},
		{
                data:   null,
                render: function ( data, type, row ) {
                    if ( data.posisi == 'col-md-push-6' && data.posisigambar == 'col-md-6 col-md-pull-6 hidden-xs') {
                        return 'Kanan';
                    }
                    else if (data.posisi == 'text-right-md' && data.posisigambar == 'col-md-6 hidden-xs'){
                    	return 'Kiri';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
		{"data":"urut"},
		// {"data":"aktif"},
		{
                data:   "aktif",
                render: function ( data, type, row ) {
                    if ( type === 'display' ) {
                        return '<input type="checkbox" class="editor-active" disabled>';
                    }
                    return data;
                },
                className: "dt-body-center"
            },
        {
                data:null,
                render: function ( data ) {
                    if ( data.lokasi != null) {
                    	return "<button class='btn btn-default pictureShow' data-lokasi='/liravel/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                    }
                    else {
                    	return'<button class="btn btn-default pictureUpload" data-id="'+data.id_sld+'" data-target="#uploadpicSlider"><i class="fa fa-upload"></i></button>';
                    }
                    return data;
                },
                className: "center"
            },
		],
		rowCallback: function ( row, data ) {
            $('input.editor-active', row).prop( 'checked', data.aktif == 1 );
        },
        "order":[[0, 'asc']],
	});
	f.on('order.dt search.dt', function(){
		f.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var g = $('#tabelProduk').DataTable({
		"ajax":"produk/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				// <li><a href='#' data-toggle='modal' data-target='#uploadpicProduk' class='pictureUploadProduk' data-id="+a.id+"><span class='fa fa-upload'></span>Ubah Gambar</a></li>
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediProduk' data-id='"+a.id_prod+"' data-jps='"+a.id_jps+"' data-produk='"+a.produk+"' data-deskripsi='"+a.deskripsi+"' class='EditProduk' id='EditProduk'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusProduk' class='HapusProduk' id='HapusProduk' data-id="+a.id_prod+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicProduk' class='pictureUploadProduk' data-id='"+a.id_prod+"'><span class='fa fa-upload'></span>Upload Gambar</a></li><li><a href='#' data-toggle='modal' data-target='#hapusProdukGambar' class='HapusProdukGambar' id='HapusProdukGambar' data-id="+a.id_prod+"><span class='fa fa-remove'></span>Hapus Gambar</a></li></ul></div></ul></div>";
			},
		},
		{"data":"id_prod"},
		{"data":"jps"},
		{"data":"produk"},
		// {"data":"lokasi"},
		{
                data:null,
                render: function ( data ) {
                    // if ( data.lokasi != null) {
                    // 	return "<button class='btn btn-default pictureShow' data-lokasi='/liravel/public/"+data.lokasi+"'><i class='fa fa-search'></i></button>";
                    // }
                    // else {
                    // 	return'<button class="btn btn-default pictureUploadProduk" data-id="'+data.id_prod+'" data-target="#uploadpicProduk"><i class="fa fa-upload"></i></button>';
                    // }
                    // return data;
                    // href='pictured?id="+data.id_prod+"'
                    // return"<a href='pictured?id="+data.id_prod+"'><button class='btn btn-default pictureShows' data-target='#pictureShows'><i class='fa fa-search'></i></button></a>";
                    // return"<input hidden type='text' id='idpicture' value='"+data.id_prod+"'><a id='pictureds'><button class='btn btn-default pictureShows' data-target='#pictureShows'><i class='fa fa-search'></i></button></a>";
                    return"<button class='btn btn-default' data-toggle='modal' data-target='#pictureShows-"+data.id_prod+"'><i class='fa fa-search'></i></button>";
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	g.on('order.dt search.dt', function(){
		g.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var h = $('#tabelProjek').DataTable({
		"ajax":"projek/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediProjek' data-id='"+a.id_prjt+"' data-jps='"+a.id_jps+"' data-projek='"+a.projek+"' data-deskripsi='"+a.deskripsi+"' class='EditProjek' id='EditProjek'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusProjek' class='HapusProjek' id='HapusProjek' data-id="+a.id_prjt+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicProjek' class='pictureUploadProjek' data-id="+a.id_prjt+"><span class='fa fa-upload'></span>Unggah Gambar</a></li></ul></div></ul></div>";
			},
		},
		{"data":"id_prjt"},
		{"data":"jps"},
		{"data":"projek"},
		// {"data":"lokasi"},
		{
                data:null,
                render: function ( data ) {
                    return"<button class='btn btn-default' data-toggle='modal' data-target='#pictureProjek-"+data.id_prjt+"'><i class='fa fa-search'></i></button>";
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	h.on('order.dt search.dt', function(){
		h.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var i = $('#tabelServis').DataTable({
		"ajax":"servis/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediServis' data-id='"+a.id_serv+"' data-jps='"+a.id_jps+"' data-servis='"+a.servis+"' data-deskripsi='"+a.deskripsi+"' data-ikon='"+a.ikon+"' class='EditServis' id='EditServis'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusServis' class='HapusServis' id='HapusServis' data-id="+a.id_serv+"><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_serv"},
		{"data":"jps"},
		{"data":"servis"},
		{"data":"ikon"},
		],
		"order":[[0, 'asc']],
	});
	i.on('order.dt search.dt', function(){
		i.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var j = $('#tabelBlog').DataTable({
		"ajax":"blog/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediBlog' data-id='"+a.id_blog+"' data-judul='"+a.judul+"' data-isi='"+a.isi_blog+"' data-penulis='"+a.rec_usr+"' class='EditBlog' id='EditBlog'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusBlog' class='HapusBlog' id='HapusBlog' data-id="+a.id_blog+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicBlog' class='pictureUploadBlog' data-id='"+a.id_blog+"'><span class='fa fa-upload'></span>Upload Gambar</a></li><li><a href='#' data-toggle='modal' data-target='#hapusBlogGambar' class='HapusBlogGambar' id='HapusBlogGambar' data-id="+a.id_blog+"><span class='fa fa-remove'></span>Hapus Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_blog"},
		{"data":"judul"},
		// {"data":"isi_blog"},
		{"data":"namauser"},
		{
                data:null,
                render: function ( data ) {
                    return"<button class='btn btn-default' data-toggle='modal' data-target='#pictureBlogs-"+data.id_blog+"'><i class='fa fa-search'></i></button>";
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	j.on('order.dt search.dt', function(){
		j.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var k = $('#tabelKlien').DataTable({
		"ajax":"klien/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediKlien' data-id='"+a.id_klien+"' data-klien='"+a.klien+"' data-alamat='"+a.alamat+"' data-telepon='"+a.telp+"' data-email='"+a.email+"' class='EditKlien'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusKlien' class='HapusKlien' data-id="+a.id_klien+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicKlien' class='pictureUploadKlien' data-id='"+a.id_klien+"'><span class='fa fa-upload'></span>Upload Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_klien"},
		{"data":"klien"},
		{"data":"alamat"},
		{"data":"telp"},
		{"data":"email"},
		{
                data:null,
                render: function ( data ) {
                	if(data.lokasi != null){
                    	return"<button class='btn btn-default pictureShow' data-toggle='modal' data-lokasi='/liravel/public/"+data.lokasi+"' data-target='#pictureShow'><i class='fa fa-search'></i></button>";
                    }
                    else{
                    	return'<button class="btn btn-default pictureUploadKlien" data-id="'+data.id_klien+'" data-target="#uploadpicKlien"><i class="fa fa-upload"></i></button>';
                    }
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	k.on('order.dt search.dt', function(){
		k.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var l = $('#tabelTesti').DataTable({
		"ajax":"testi/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediTesti' data-id='"+a.id_testi+"' data-judul='"+a.judul+"' data-klien='"+a.id_klien+"' data-nama='"+a.nama+"' data-testimoni='"+a.testimoni+"' class='EditTesti' id='EditTesti'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusTesti' class='HapusTesti' id='HapusTesti' data-id="+a.id_testi+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicTesti' class='pictureUploadTesti' data-id='"+a.id_testi+"'><span class='fa fa-upload'></span>Upload Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_testi"},
		{"data":"klien"},
		{"data":"nama"},
		{"data":"testimoni"},
		{
                data:null,
                render: function ( data ) {
                	if(data.lokasi != null){
                    	return"<button class='btn btn-default pictureShow' data-toggle='modal' data-lokasi='/liravel/public/"+data.lokasi+"' data-target='#pictureShow'><i class='fa fa-search'></i></button>";
                	}
                	else{
                		return'<button class="btn btn-default pictureUploadTesti" data-id="'+data.id_testi+'" data-target="#uploadpicTesti"><i class="fa fa-upload"></i></button>';
                	}
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	l.on('order.dt search.dt', function(){
		l.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var m = $('#tabelPartner').DataTable({
		"ajax":"partner/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediPartner' data-id='"+a.id_partner+"' data-partner='"+a.partner+"' data-alamat='"+a.alamat+"' data-telepon='"+a.telp+"' data-email='"+a.email+"' class='EditPartner'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusPartner' class='HapusPartner' data-id="+a.id_partner+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicPartner' class='pictureUploadPartner' data-id='"+a.id_partner+"'><span class='fa fa-upload'></span>Upload Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_partner"},
		{"data":"partner"},
		{"data":"alamat"},
		{"data":"telp"},
		{"data":"email"},
		{
                data:null,
                render: function ( data ) {
                	if(data.lokasi != null){
                		return"<button class='btn btn-default pictureShow' data-toggle='modal' data-lokasi='/liravel/public/"+data.lokasi+"' data-target='#pictureShow'><i class='fa fa-search'></i></button>";	
                	}
                	else{
                		return'<button class="btn btn-default pictureUploadPartner" data-id="'+data.id_partner+'" data-target="#uploadpicPartner"><i class="fa fa-upload"></i></button>';
                	}
                    return data;
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	m.on('order.dt search.dt', function(){
		m.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var n = $('#tabelUnduhan').DataTable({
		"ajax":"unduhan/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediUnduhan' data-id='"+a.id_unduh+"' data-judul='"+a.judulunduh+"' data-link='"+a.linkunduh+"' class='EditUnduhan'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusUnduhan' class='HapusUnduhan' data-id="+a.id_unduh+"><span class='fa fa-remove'></span>Hapus</a></li></ul></div>";
			},
		},
		{"data":"id_unduh"},
		{"data":"judulunduh"},
		{"data":"linkunduh"},
		],
		"order":[[0, 'asc']],
	});
	n.on('order.dt search.dt', function(){
		n.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
	var o = $('#tabelUsers').DataTable({
		"ajax":"users/array",
		"language":{"url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json"},
		"columns":[
		{bSortable: false,
			data:null,
			className: "center",
			render: function(a){
				return"<div class='dropdown'><button class='btn btn-default dropdown-toggle' data-toggle='dropdown'><li class='fa fa-cog'></li></button><ul class='dropdown-menu'><li><a href='#' data-toggle='modal' data-target='#ediUsers' data-id='"+a.id_usr+"' data-email='"+a.email+"' data-nama='"+a.namauser+"' data-status='"+a.status+"' data-jenis='"+a.jnsuser+"' class='EditUsers'><span class='fa fa-edit'></span>Edit</a></li><li><a href='#' data-toggle='modal' data-target='#hapusUsers' class='HapusUsers' data-id="+a.id_usr+"><span class='fa fa-remove'></span>Hapus</a></li><li><a href='#' data-toggle='modal' data-target='#uploadpicUsers' class='pictureUploadUsers' data-id='"+a.id_usr+"'><span class='fa fa-upload'></span>Upload Gambar</a></li></ul></div>";
			},
		},
		{"data":"id_usr"},
		{"data":"namauser"},
		{"data":"email"},
		{"data":"jenisuser"},
		{
                data:   null,
                render: function ( data, type, row ) {
                    if ( data.status == 1 ) {
                        return '<input type="checkbox" checked class="editor-active" disabled>';
                    }
                    else{
                    	return '<input type="checkbox" class="editor-active" disabled>';	
                    }
                    return data;
                },
                className: "dt-body-center"
            },
		{
                data:null,
                render: function ( data ) {
                	if(data.lokasi != null){
                		return"<button class='btn btn-default pictureShow' data-toggle='modal' data-lokasi='/liravel/public/"+data.lokasi+"' data-target='#pictureShow'><i class='fa fa-search'></i></button>";	
                	}
                	else{
                		return'<button class="btn btn-default pictureUploadUsers" data-id="'+data.id_usr+'" data-target="#uploadpicUsers"><i class="fa fa-upload"></i></button>';
                	}
                    return data;
                },
                className: "center"
            },
		],
		"order":[[0, 'asc']],
	});
	o.on('order.dt search.dt', function(){
		o.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell, i){
			cell.innerHTML = i+1;
		});
	}).draw();
});