<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGbrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gbr', function (Blueprint $table) {
            $table->increments('id_gambar');
            $table->string('lokasi', 255);
            $table->integer('id_prof')->nullable();
            $table->integer('id_prod')->nullable();
            $table->integer('id_prjt')->nullable();
            $table->integer('id_klien')->nullable();
            $table->integer('id_testi')->nullable();
            $table->integer('id_blog')->nullable();
            $table->integer('id_sld')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gbr');
    }
}
