<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('about', 'WebView@bout');
Route::get('blog/{id}', 'WebView@blog');
Route::get('elements', 'WebView@elements');
Route::get('index', 'WebView@show');

Route::get('admin', 'uploade@admin');
Route::get('admin/inisialisasi', 'uploade@inisialisasi');
Route::get('admin/pps', 'uploade@pps');
Route::get('up', 'uploade@uploadFile');
Route::get('/changePassword', 'uploade@showChangePasswordForm');
Route::get('admin/produk', 'uploade@produk');
Route::get('admin/projek', 'uploade@projek');
Route::get('admin/servis', 'uploade@servis');
Route::get('admin/blog', 'uploade@blog');
Route::get('admin/klientesti', 'uploade@klientesti');
Route::get('admin/partner', 'uploade@partner');
Route::get('admin/unduhan', 'uploade@unduhan');
Route::get('admin/users', 'uploade@userWEB');

Route::get('admin/kontak', 'uploade@arrayKontak');
Route::get('admin/sosmed', 'uploade@arraySosmed');
Route::get('admin/office', 'uploade@arrayAlamat');
Route::get('admin/jenis', 'uploade@arrayJenis');
Route::get('admin/video', 'uploade@arrayVideo');
Route::get('admin/slider', 'uploade@arraySlider');
Route::get('admin/produk/array', 'uploade@arrayProduk');
Route::get('admin/projek/array', 'uploade@arrayProjek');
Route::get('admin/servis/array', 'uploade@arrayServis');
Route::get('admin/blog/array', 'uploade@arrayBlog');
Route::get('admin/klien/array', 'uploade@arrayKlien');
Route::get('admin/testi/array', 'uploade@arrayTesti');
Route::get('admin/partner/array', 'uploade@arrayPartner');
Route::get('admin/unduhan/array', 'uploade@arrayUnduhan');
Route::get('admin/users/array', 'uploade@arrayUserWEB');
// Route::post('admin/pictured/{id}', 'uploade@arrayPicture');

// Route::post('upload', 'uploade@StoreUploadFile');
Route::post('tambahKontak', 'uploade@tambahKontak');
Route::post('tambahSosmed', 'uploade@tambahSosmed');
Route::post('tambahOffice', 'uploade@tambahOffice');
Route::post('admin/tambahJenis', 'uploade@tambahJenis');
Route::post('admin/tambahVideo', 'uploade@tambahVideo');
Route::post('admin/tambahSlider', 'uploade@tambahSlider');
Route::post('/changePassword','uploade@changePassword')->name('changePassword');
Route::post('admin/uploadpicturesld', 'uploade@uploadSlider');
Route::post('admin/tambahProduk', 'uploade@tambahProduk');
Route::post('admin/uploadpictureproduk', 'uploade@uploadProduk');
Route::post('admin/uploadpictureproduks', 'uploade@uploadProduks');
Route::post('admin/tambahProjek', 'uploade@tambahProjek');
Route::post('admin/uploadpictureprojek', 'uploade@uploadProjek');
Route::post('admin/tambahServis', 'uploade@tambahServis');
Route::post('admin/tambahBlog', 'uploade@tambahBlog');
Route::post('admin/uploadpictureblog', 'uploade@uploadBlog');
Route::post('admin/tambahKlien', 'uploade@tambahKlien');
Route::post('admin/uploadpictureklien', 'uploade@uploadKlien');
Route::post('admin/tambahTesti', 'uploade@tambahTesti');
Route::post('admin/uploadpicturetesti', 'uploade@uploadTesti');
Route::post('admin/tambahPartner', 'uploade@tambahPartner');
Route::post('admin/uploadpicturepartner', 'uploade@uploadPartner');
Route::post('admin/tambahUnduhan', 'uploade@tambahUnduhan');
Route::post('admin/tambahUsers', 'uploade@tambahUsers');
Route::post('admin/uploadpictureusers', 'uploade@uploadUsers');
// Route::get('admin/produk/image', 'uploade@getImageID');

Route::patch('editprof/{id}', 'uploade@editProf');
Route::patch('editKontak/{id}', 'uploade@editKontak');
Route::patch('editSosmed/{id}', 'uploade@editSosmed');
Route::patch('editOffice/{id}', 'uploade@editOffice');
Route::patch('admin/editJenis/{id}', 'uploade@editJenis');
Route::patch('admin/editVideo/{id}', 'uploade@editVideo');
Route::patch('admin/editSlider/{id}', 'uploade@editSlider');
Route::patch('admin/editProduk/{id}', 'uploade@editProduk');
Route::patch('admin/editProjek/{id}', 'uploade@editProjek');
Route::patch('admin/editServis/{id}', 'uploade@editServis');
Route::patch('admin/editBlog/{id}', 'uploade@editBlog');
Route::patch('admin/editKlien/{id}', 'uploade@editKlien');
Route::patch('admin/editTesti/{id}', 'uploade@editTesti');
Route::patch('admin/editPartner/{id}', 'uploade@editPartner');
Route::patch('admin/editUnduhan/{id}', 'uploade@editUnduhan');
Route::patch('admin/editUsers/{id}', 'uploade@editUsers');

Route::delete('hapusKontak/{id}', 'uploade@hapusKontak');
Route::delete('hapusSosmed/{id}', 'uploade@hapusSosmed');
Route::delete('hapusOffice/{id}', 'uploade@hapusOffice');
Route::delete('admin/hapusJenis/{id}', 'uploade@hapusJenis');
Route::delete('admin/hapusVideo/{id}', 'uploade@hapusVideo');
Route::delete('admin/hapusSlider/{id}', 'uploade@hapusSlider');
Route::delete('admin/hapusProduk/{id}', 'uploade@hapusProduk');
Route::delete('admin/hapusProdukGambar/{id}', 'uploade@hapusProdukGambar');
Route::delete('admin/hapusProjek/{id}', 'uploade@hapusProjek');
Route::delete('admin/hapusServis/{id}', 'uploade@hapusServis');
Route::delete('admin/hapusBlog/{id}', 'uploade@hapusBlog');
Route::delete('admin/hapusKlien/{id}', 'uploade@hapusKlien');
Route::delete('admin/hapusTesti/{id}', 'uploade@hapusTesti');
Route::delete('admin/hapusPartner/{id}', 'uploade@hapusPartner');
Route::delete('admin/hapusUnduhan/{id}', 'uploade@hapusUnduhan');
Route::delete('admin/hapusUsers/{id}', 'uploade@hapusUsers');

Auth::routes();

Route::get('/home', 'uploade@index')->name('home');
